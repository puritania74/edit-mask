const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const babelOptions = {
    'presets': [
        'stage-0',
        [
            'es2015',
            {
                'modules': false
            }
        ]
    ]
};

let output = path.resolve(__dirname, 'dist');

module.exports = function(options) {
    options = defaults(options, {
        development: false
    });

    let config = {
        entry: path.resolve(__dirname, 'src/index.ts'),
        output: {
            path: output,
            filename: 'edit-mask.js',
            library: 'editMask',
            libraryTarget: 'umd'
        },
        resolve: {
            extensions: ['.ts', '.js', '.json']
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: babelOptions
                        },
                        {
                            loader: 'ts-loader'
                        }
                    ]
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: babelOptions
                        }
                    ]
                }
            ]
        },
        watch: options.development === true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000,
            ignored: /node_modules/
        },
        devtool: options.development ? 'source-map' : '',
        plugins: [
            new webpack.LoaderOptionsPlugin({
                debug: true
            }),
            new webpack.BannerPlugin({
                raw: true,
                banner: ';'
            })
        ]
    };

    if (!options.development) {
        config.plugins.push(new webpack.optimize.UglifyJsPlugin());
        config.plugins.push(new CleanWebpackPlugin(output))
    }

    return config;
};

function defaults(obj, defaultValues) {
    obj = obj || {};

    Object
        .keys(defaultValues)
        .forEach(key => defaultTo(obj, key, defaultValues[key]));

    return obj;
}

function defaultTo(obj, key, defaultValue) {
    if (typeof obj[key] === 'undefined' || obj[key] === null) {
        obj[key] = defaultValue;
    }
}
