export interface IMonthLocalization {
    full: Array<String>

}
export interface ILocalization {

    numberDecimalDigits: number
    numberDecimalSeparator: string
    numberGroupSeparator: string
    numberGroupSize: number
    numberNegativePattern: string
    numberPositivePattern: string

    percentDecimalDigits: number
    percentDecimalSeparator: string
    percentGroupSeparator:string
    percentSymbol:string
    percentNegativePattern:string
    percentPositivePattern: string

    currencyDecimalDigits: number
    currencyDecimalSeparator:string
    currencyGroupSeparator:string
    currencySymbol:string
    currencyNegativePattern:string
    currencyPositivePattern:string

    negativeInfinitySymbol:string
    positiveInfinitySymbol:string
    NaNSymbol:string
    months: IMonthLocalization
    letter: string

}