import { INumberParserOptions, INumberPartParser, INumberPartParserResult } from "../base/interfaces";
export class FractionalPartParser implements INumberPartParser {


    constructor() {

    }

    parse( text: string, start: number, options:INumberParserOptions ): INumberPartParserResult {
        let regexp = new RegExp('[\\d]{1,' + options.decimalDigits + '}');
        let result:INumberPartParserResult = null;

        let matches = text.substr(start).match(regexp);

        if (matches) {
            result = {
                text: matches[0],
                start: start + matches.index,
                end: start + matches.index + matches[0].length
            }
        }
        return result;
    }

}
