import { INumberParserOptions, INumberPartParser, INumberPartParserResult } from "../base/interfaces";
export class IntegralPartParser implements INumberPartParser {

    constructor(protected groupSeparator:string,
                protected decimalSeparator:string
    ) {

    }

    parse( text: string, start: number, options: INumberParserOptions ): INumberPartParserResult {
        let i = start;
        let rDigit = /\d/;
        let found = '';

        let groupSeparator = this.groupSeparator;

        while(i < text.length) {
            if (text.indexOf(groupSeparator, i) === i) {
                found += groupSeparator;
                i += groupSeparator.length;
            } else if (text[i].match(rDigit)) {
                found += text[i];
                i+= 1;
            } else if (text.indexOf(this.decimalSeparator, i) === i){
                break;
            } else {
                i++;
            }
        }

        return {
            text: found,
            start: i - found.length,
            end: i
        };
    }

}