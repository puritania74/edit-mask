import { INumberParserOptions, INumberPartParser, INumberPartParserResult } from "../base/interfaces";
export class DecimalSeparatorParser implements INumberPartParser {

    constructor(protected decimalSeparator: string) {

    }

    parse( text: string, start: number, options: INumberParserOptions ): INumberPartParserResult {
        let decimalSeparator = this.decimalSeparator;
        let result:INumberPartParserResult = null;

        if (text.indexOf(decimalSeparator, start) === start) {
            result = {
                text: decimalSeparator,
                start: start,
                end: start + decimalSeparator.length
            };
        }
        return result;
    }

}