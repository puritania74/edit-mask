import { BaseNumberParser } from "./base/BaseNumberParser";
import { Locale } from "../Locale";
import { IntegralPartParser } from "./common/IntegralPartParser";
import { DecimalSeparatorParser } from "./common/DecimalSeparatorParser";
import { FractionalPartParser } from "./common/FractionalPartParser";
export class CurrencyParser extends BaseNumberParser {

    protected getDecimalSeparator(): string {
        return this.locale.currencyDecimalSeparator;
    }

    constructor(protected locale:Locale) {
        super(locale);
        this.integralPartParser = new IntegralPartParser(locale.currencyGroupSeparator, locale.currencyDecimalSeparator);
        this.decimalSeparatorParser = new DecimalSeparatorParser(locale.currencyDecimalSeparator);
        this.fractionalPartParser = new FractionalPartParser();
    }

}