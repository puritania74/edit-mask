import { Locale } from "../Locale";
import { BaseNumberParser } from "./base/BaseNumberParser";
import { IntegralPartParser } from "./common/IntegralPartParser";
import { DecimalSeparatorParser } from "./common/DecimalSeparatorParser";
import { FractionalPartParser } from "./common/FractionalPartParser";

export class PercentParser extends BaseNumberParser {

    protected getDecimalSeparator(): string {
        return this.locale.percentDecimalSeparator;
    }

    constructor(protected locale:Locale) {
        super(locale);
        this.integralPartParser = new IntegralPartParser(locale.percentGroupSeparator, locale.percentDecimalSeparator);
        this.decimalSeparatorParser = new DecimalSeparatorParser(locale.percentDecimalSeparator);
        this.fractionalPartParser = new FractionalPartParser();
    }

}