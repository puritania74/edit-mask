import { BaseNumberParser } from "./base/BaseNumberParser";
import { Locale } from "../Locale";
import { IntegralPartParser } from "./common/IntegralPartParser";
import { DecimalSeparatorParser } from "./common/DecimalSeparatorParser";
import { FractionalPartParser } from "./common/FractionalPartParser";

export class NumberParser extends BaseNumberParser {


    protected getDecimalSeparator(): string {
        return this.locale.numberDecimalSeparator;
    }

    constructor(protected locale:Locale) {
        super(locale);
        this.integralPartParser = new IntegralPartParser(locale.numberGroupSeparator, locale.numberDecimalSeparator);
        this.decimalSeparatorParser = new DecimalSeparatorParser(locale.numberDecimalSeparator);
        this.fractionalPartParser = new FractionalPartParser();
    }

}
