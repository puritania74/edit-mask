export interface INumberPartParser {
    parse(text: string, start: number, options: INumberParserOptions): INumberPartParserResult
}

export interface INumberPartParserResult {
    text: string
    start: number
    end: number
}

export interface IParserOptions {
    [name: string]: any
}



export interface INumberParserOptions extends IParserOptions{
    isNegative: boolean
    decimalDigits: number
}

export interface IParser {
    parse(text: string, options: IParserOptions, start: number):IParserResponse
    isComplete(text: string, decimalDigits:number):boolean
}

export interface IParserResponse {
    integralPart: string
    fractionalPart: string
    separatorPart?: string
    input: string
}

export interface INumberParserResponse extends IParserResponse{

}