import { Locale } from "../../Locale";
import { INumberParserOptions, INumberParserResponse, INumberPartParser, IParser } from "./interfaces";
import { DecimalSeparatorParser } from "../common/DecimalSeparatorParser";
import { IntegralPartParser } from "../common/IntegralPartParser";
import { FractionalPartParser } from "../common/FractionalPartParser";


export abstract class BaseNumberParser implements IParser {

    decimalSeparatorParser: DecimalSeparatorParser;
    integralPartParser: IntegralPartParser;
    fractionalPartParser: FractionalPartParser;

    constructor(protected locale:Locale) {

    }

    protected abstract getDecimalSeparator():string


    parse(text: string, options: INumberParserOptions, start: number = 0):INumberParserResponse {
        let input = text.substr(start);
        return options.decimalDigits > 0 ?
            this.parseFloatNumber(input, options) : this.parseIntegerNumber(input, options);
    }

    isComplete(text: string, decimalDigits:number):boolean {
        let isComplete:boolean;
        let decimalSeparator = this.getDecimalSeparator();

        if (decimalDigits === 0) {
            let digits = text.replace(/[^\d]/g, '');
            isComplete = digits.length > 0;
        } else {
            //Шаблон заполнен если заполнена целая и дробная часть числа
            let [int, fract] = text.split(decimalSeparator).map(t => t.replace(/[^\d]/g, ''));
            //@TODO fract.length === decimalDigits или fract.length > 0 ?
            //@TODO check  fract.length > 0 ?
            isComplete = int.length > 0;
        }
        return isComplete;
    }

    protected parseFloatNumber(text:string, options: INumberParserOptions):INumberParserResponse {
        let result:INumberParserResponse = {
            separatorPart: '',
            integralPart: '',
            fractionalPart: '',
            input: text
        };

        let parts:Array<string> = this.run(text, [
            this.integralPartParser,
            this.decimalSeparatorParser,
            this.fractionalPartParser
        ], options);

        result.integralPart = parts[0];
        result.separatorPart = parts[1];
        result.fractionalPart = parts[2];
        result.input = parts.join('');

        return result;
    }

    protected parseIntegerNumber(text:string, options: INumberParserOptions): INumberParserResponse {
        let result:INumberParserResponse = {
            integralPart: '',
            fractionalPart: '',
            input: text
        };

        let parts:Array<string> = this.run(text, [
            this.integralPartParser
        ], options);

        result.integralPart = parts[0];
        result.input = parts.join('');

        return result;
    }

    protected run(text: string, parsers: Array<INumberPartParser>, options: INumberParserOptions): Array<string> {
        let data:Array<string> = parsers.map( p => '');

        // let template = options.isNegative ? this.locale.numberNegativePattern : this.locale.numberPositivePattern;
        // let [prefix, suffix] = template.split('n');
        //
        //@TODO strip template wrapper

        for (let i = 0, index = 0; i < parsers.length; i = i + 1) {
            let result = parsers[i].parse(text, index, options);
            if (result === null) {
                break;
            }
            index = result.end;
            data[i] = result.text || '';
        }

        return data;
    }

}
