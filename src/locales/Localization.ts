import { Ru } from "./localizations/ru";
import { En } from "./localizations/en";
import { ILocalization } from "./ILocalization";

export class Localization {

    getLocalization(lang: string):ILocalization {
        lang = lang.toUpperCase();
        let localization:ILocalization;
        switch (lang) {
            case 'RU':
            case 'RU-RU':
                localization = new Ru();
                break;
            case 'EN':
            case 'EN-US':
                localization = new En();
                break;
            default:
                localization = this.getDefaultLocalization();
        }

        return localization;
    }

    getDefaultLocalization() {
        return new Ru();
    }


}