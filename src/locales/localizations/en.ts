import { ILocalization } from "../ILocalization";

export class En implements ILocalization {
    letter = 'a-zA-Z';

    numberDecimalDigits = 2;
    numberDecimalSeparator = '.';
    numberGroupSeparator = ',';
    numberGroupSize = 3;
    numberNegativePattern = '-n';
    numberPositivePattern = 'n';

    percentDecimalDigits = 2;
    percentDecimalSeparator = '.';
    percentGroupSeparator = ',';
    percentSymbol = '%';
    percentNegativePattern = '-p %';
    percentPositivePattern = 'p %';

    currencyDecimalDigits = 2;
    currencyDecimalSeparator = '.';
    currencyGroupSeparator = ',';
    currencySymbol = '$';
    currencyNegativePattern = '($c)';
    currencyPositivePattern = '$c';

    negativeInfinitySymbol = '-Infinity';
    positiveInfinitySymbol = 'Infinity';
    NaNSymbol = 'NaN';

    months = {
        full: 'January,February,March,April,May,June,July,August,September,October,November,December'.split(',')
    };

}