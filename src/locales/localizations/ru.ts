import { ILocalization } from "../ILocalization";

export class Ru implements ILocalization {
    letter = 'а-яА-Яa-zA-Z';

    numberDecimalDigits = 2;
    numberDecimalSeparator = ',';
    numberGroupSeparator = ' ';
    numberGroupSize = 3;
    numberNegativePattern = '-n';
    numberPositivePattern = 'n';

    percentDecimalDigits = 2;
    percentDecimalSeparator = ',';
    percentGroupSeparator = ' ';
    percentSymbol = '%';
    percentNegativePattern = '-p %';
    percentPositivePattern = 'p %';

    currencyDecimalDigits = 2;
    currencyDecimalSeparator = ',';
    currencyGroupSeparator = ' ';
    currencySymbol = 'р.';
    currencyNegativePattern = '-c$';
    currencyPositivePattern = 'c$';

    negativeInfinitySymbol = '-бесконечность';
    positiveInfinitySymbol = 'бесконечность';
    NaNSymbol = 'NaN';

    months = {
        full: 'январь,февраль,март,апрель,май,июнь,июль,август,сентябрь,октябрь,ноябрь,декабрь'.split(',')
    };

}