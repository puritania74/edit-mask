import { NumberParser } from "./parsers/NumberParser";
import { NumberFormatter } from "./formatters/NumberFormatter";
import { ILocalization, IMonthLocalization } from "./ILocalization";
import { Localization } from "./Localization";




export class Locale implements ILocalization{
    letter:string;

    numberDecimalDigits:number;
    numberDecimalSeparator:string;
    numberGroupSeparator:string;
    numberGroupSize: number;
    numberNegativePattern: string;
    numberPositivePattern: string;
    percentDecimalDigits: number;
    percentDecimalSeparator: string;
    percentGroupSeparator: string;
    percentSymbol: string;
    percentNegativePattern: string;
    percentPositivePattern: string;
    currencyDecimalDigits: number;
    currencyDecimalSeparator: string;
    currencyGroupSeparator: string;
    currencySymbol: string;
    currencyNegativePattern: string;
    currencyPositivePattern: string;
    negativeInfinitySymbol: string;
    positiveInfinitySymbol: string;
    NaNSymbol: string;
    months: IMonthLocalization;

    numberParser:NumberParser = new NumberParser(this);
    numberFormatter:NumberFormatter = new NumberFormatter(this);


    constructor(lang:string) {
        let localization = new Localization();
        let current = localization.getLocalization(lang);

        Object.keys(current).forEach(name => this[name] = current[name]);
    }

}