import { INumberFormatterOptions, NumberFormatter } from "./NumberFormatter";

export class CurrencyFormatter extends NumberFormatter {

    getDecimalSeparator():string {
        return this.locale.currencyDecimalSeparator;
    }

    getNegativePattern():string {
        return this.locale.currencyNegativePattern;
    }

    getPositivePattern():string {
        return this.locale.currencyPositivePattern;
    }

    getGroupSeparator():string {
        return this.locale.currencyGroupSeparator;
    }

    getCurrencySymbol():string {
        return this.locale.currencySymbol;
    }

    protected getWrapper(options: INumberFormatterOptions):Array<string> {
        let pattern = this.getPattern(options.isNegative);

        return pattern.split(/c/i).map(t => t.replace('$', this.getCurrencySymbol()));
    }
}