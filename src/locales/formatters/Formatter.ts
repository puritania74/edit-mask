
export interface IFormatterOptions {
    [name:string]: any
}

export interface IFormatter {
    formatEmpty(options?:IFormatterOptions):string
    format(text: string, options?:IFormatterOptions):string
    formatInput(input:string, options?:IFormatterOptions):string
}