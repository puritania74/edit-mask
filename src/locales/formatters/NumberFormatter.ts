import { Locale } from "../Locale";
import { padStart } from "../../utils/padStart";
import { padEnd } from "../../utils/padEnd";
import { IFormatter, IFormatterOptions } from "./Formatter";
import { isEmpty } from "../../utils/isEmpty";


export interface INumberFormatterOptions extends IFormatterOptions {
    decimalDigits: number
    isNegative: boolean
    blank: string
}

export class NumberFormatter implements IFormatter{

    constructor(protected locale: Locale) {

    }
    
    getDecimalSeparator():string {
        return this.locale.numberDecimalSeparator;
    }

    getNegativePattern():string {
        return this.locale.numberNegativePattern;
    }

    getPositivePattern():string {
        return this.locale.numberPositivePattern;
    }

    getGroupSeparator():string {
        return this.locale.numberGroupSeparator;
    }

    getGroupSize():number {
        return this.locale.numberGroupSize;
    }

    formatEmpty(options?: INumberFormatterOptions):string {
        let blank = options.blank;
        let formattedParts:Array<string> = [padStart('', 1, blank)];

        if (options.decimalDigits > 0) {
            formattedParts.push(padStart('', options.decimalDigits, blank));
        }

        let formattedText = formattedParts.join(this.getDecimalSeparator());

        return this.wrapSignPattern(formattedText, options);
    }

    format( text: string, options?: INumberFormatterOptions ): string {
        let formattedParts:Array<string> = [];
        let {integral, fractional} = this.splitNumberByParts(text);

        formattedParts.push(this.formatGroup(integral));

        if (options.decimalDigits > 0) {
            fractional = padEnd(fractional || '', options.decimalDigits, '0');
            formattedParts.push(fractional.substr(0, options.decimalDigits));
        }

        let formattedText = formattedParts.join(this.getDecimalSeparator());
        return this.wrapSignPattern(formattedText, options);
    }

    formatValue(value: number, options?: INumberFormatterOptions) :string {

        if (isEmpty(value)) {
            return this.formatEmpty(options);
        } else {
            let text = value.toFixed(options.decimalDigits)
                .split('.')
                .join(this.getDecimalSeparator());

            return this.format(text, options);
        }
    }

    formatInput(input:string, options: INumberFormatterOptions):string {
        let formattedParts:Array<string> = [];
        let {integral, fractional} = this.splitNumberByParts(input);
        let decimalDigits = options.decimalDigits;

        if (integral.length > 0) {
            formattedParts.push(this.formatGroup(integral));
        } else {
            formattedParts.push(padStart('', 1, options.blank));
        }

        if (options.decimalDigits > 0) {
            fractional = fractional || '';
            formattedParts.push(padEnd(fractional.substr(0, decimalDigits), decimalDigits, '0'));
        }

        let formattedText =  formattedParts.join(this.getDecimalSeparator());
        return this.wrapSignPattern(formattedText, options);
    }

    protected getPattern(isNegative:boolean) :string {
        return isNegative ? this.getNegativePattern() : this.getPositivePattern();
    }

    protected getWrapper(options: INumberFormatterOptions):Array<string> {
        let pattern = this.getPattern(options.isNegative);
        return pattern.split(/n/i);
    }

    protected wrapSignPattern(text:string, options: INumberFormatterOptions):string {
        let wrapper = this.getWrapper(options);

        return wrapper.join(text);
    }

    protected splitNumberByParts(text:string): {integral:string, fractional:string} {
        let [integral, fractional] = text.split(this.getDecimalSeparator())
            .map(t => t.replace(/[^\d]/g, ''));

        return {integral, fractional};
    }

    protected formatGroup(text:string):string {
        let groups = [];
        let groupSize = this.getGroupSize();
        let groupSeparator = this.getGroupSeparator();
        for (let t = text; t.length >0; t = t.substr(0, t.length - groupSize)) {
            groups.push(t.substr(-groupSize));
        }

        return groups.reverse().join(groupSeparator);
    }
}