import { INumberFormatterOptions, NumberFormatter } from "./NumberFormatter";

export class PercentFormatter extends NumberFormatter {

    getDecimalSeparator():string {
        return this.locale.percentDecimalSeparator;
    }

    getNegativePattern():string {
        return this.locale.percentNegativePattern;
    }

    getPositivePattern():string {
        return this.locale.percentPositivePattern;
    }

    getGroupSeparator():string {
        return this.locale.percentGroupSeparator;
    }

    getPercentSymbol():string {
        return this.locale.percentSymbol;
    }

    protected getWrapper(options: INumberFormatterOptions):Array<string> {
        let pattern = this.getPattern(options.isNegative);

        return pattern.split(/p/i).map(t => t.replace('%', this.getPercentSymbol()));
    }
}