import { FixedWidthPattern } from "../../common/FixedWidthPattern";

export class YearPattern extends FixedWidthPattern<number>{

    constructor(locale, template:string) {
        super(locale, template, 'year', {min: 1, max: 9999, width: 4});
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let year = new Date(value).getUTCFullYear();
            this.setText(this.formatDecimal(year.toString()));
        }
    }

}