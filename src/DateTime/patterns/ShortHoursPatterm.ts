import { isCompleteHours } from "./utils/isCompleteHours";
import { FloatWidthPattern } from "../../common/FloatWidthPattern";
import { getHours } from "./utils/getHours";

export class ShortHoursPattern extends FloatWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'hours', {min: 0, max: 23, minWidth: 1, maxWidth: 2});
    }

    getIsComplete(): boolean {
        return isCompleteHours(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let text = getHours(value);
            this.setText(text);
        }
    }


}