import { FixedWidthPattern } from "../../common/FixedWidthPattern";
import { isCompleteDay } from "./utils/isCompleteDay";
import { getDay } from "./utils/getDay";

export class DayPattern extends FixedWidthPattern<number>{

    constructor(locale, template:string) {
        super(locale, template, 'day', {min: 1, max: 31, width: 2});
    }

    getIsComplete(): boolean {
        return isCompleteDay(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let text = getDay(value);
            this.setText(this.formatDecimal(text));
        }
    }

}