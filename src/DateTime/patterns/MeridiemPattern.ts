import { ListPattern } from "../../common/ListPattern";

const AM_DESIGNATOR:string = 'AM';
const PM_DESIGNATOR:string = 'PM';

export class MeridiemPattern extends ListPattern<number> {

    constructor(locale, template) {
        super(locale, template, 'meridiem', [AM_DESIGNATOR, PM_DESIGNATOR]);
    }

    applyValue( value: number ) {
        let text;

        if (value === null || typeof value === 'undefined') {
            text = '';
        } else {
            let date = new Date(value);
            let hours = date.getUTCHours();

            text = hours >= 12 ? PM_DESIGNATOR : AM_DESIGNATOR;
        }

        this.setText(text);
    }

}
