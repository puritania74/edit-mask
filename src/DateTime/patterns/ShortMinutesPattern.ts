import { FloatWidthPattern } from "../../common/FloatWidthPattern";
import { isCompleteMinutes } from "./utils/isCompleteMinutes";

export class ShortMinutesPattern extends FloatWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'minutes', {min: 0, max: 59, minWidth: 1, maxWidth: 2});
    }

    getIsComplete(): boolean {
        return isCompleteMinutes(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let minutes = new Date(value).getUTCMinutes();
            this.setText(minutes.toString());
        }
    }


}