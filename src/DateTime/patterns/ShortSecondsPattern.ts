import { FloatWidthPattern } from "../../common/FloatWidthPattern";
import { isCompleteSeconds } from "./utils/isCompleteSeconds";

export class ShortSecondsPattern extends FloatWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'seconds', {min: 0, max: 59, minWidth: 1, maxWidth: 2});
    }

    getIsComplete(): boolean {
        return isCompleteSeconds(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let seconds = new Date(value).getUTCSeconds();
            this.setText(seconds.toString());
        }
    }

}