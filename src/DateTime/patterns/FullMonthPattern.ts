import { ListPattern } from "../../common/ListPattern";

const MONTHS = 'январь,февраль,март,апрель,май,июнь,июль,август,сентябрь,октябрь,ноябрь,декабрь'.split(',');

export class FullMonthPattern extends ListPattern<number> {

    constructor(locale, template) {
        super(locale, template, 'month', locale.months.full);
    }

    applyValue( value: number ) {
        let text;

        if (value === null || typeof value === 'undefined') {
            text = '';
        } else {
            let date = new Date(value);
            let month = date.getUTCMonth();
            let months = this.locale.months.full;
            text = months[month];
        }

        this.setText(text);
    }

}
