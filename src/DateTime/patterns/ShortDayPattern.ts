import { FloatWidthPattern } from "../../common/FloatWidthPattern";
import { isCompleteDay } from "./utils/isCompleteDay";
import { getDay } from "./utils/getDay";
export class ShortDayPattern extends FloatWidthPattern<number> {

    constructor(locale, template: string) {
        super(locale, template, 'day', {min: 1, max: 31, minWidth: 1, maxWidth: 2});
    }

    getIsComplete(): boolean {
        return isCompleteDay(this.getText());
    }

    applyValue( value: number ) {
        this.setText(getDay(value));
    }
}