import { FixedWidthPattern } from "../../common/FixedWidthPattern";
import { isCompleteHours } from "./utils/isCompleteHours";
import { getHours12 } from "./utils/getHours12";

export class Hours12Pattern extends FixedWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'hours', {min: 1, max: 12, width: 2});
    }

    getIsComplete(): boolean {
        return isCompleteHours(this.getText());
    }

    applyValue( value: number ) {
        let text = getHours12(value);
        this.setText(this.formatDecimal(text));
    }
}