import { FixedWidthPattern } from "../../common/FixedWidthPattern";
import { isCompleteMinutes } from "./utils/isCompleteMinutes";

export class MinutesPattern extends FixedWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'minutes', {min: 0, max: 59, width: 2});
    }

    getIsComplete(): boolean {
        return isCompleteMinutes(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let minutes = new Date(value).getUTCMinutes();
            this.setText(this.formatDecimal(minutes.toString()));
        }
    }

}