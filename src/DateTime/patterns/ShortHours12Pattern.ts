
import { isCompleteHours } from "./utils/isCompleteHours";
import { FloatWidthPattern } from "../../common/FloatWidthPattern";
import { getHours12 } from "./utils/getHours12";

export class ShortHours12Pattern extends FloatWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'hours', {min: 1, max: 12, minWidth: 1, maxWidth: 2});
    }

    getIsComplete(): boolean {
        return isCompleteHours(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let text = getHours12(value);
            this.setText(text);
        }
    }


}