export function isCompleteSeconds(text: string):boolean {
    let digits = text.replace(/[^\d]/g, '');
    let seconds = parseInt(digits, 10);

    return seconds >= 6;
}