export function getHours(value: number) :string{

    let text :string;

    if (value === null || typeof value === 'undefined') {
        text = '';
    } else {
        let hours = new Date(value).getUTCHours();
        text = hours.toString();
    }

    return text;
}