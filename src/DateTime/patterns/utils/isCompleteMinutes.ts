export function isCompleteMinutes(text: string):boolean {
    let digits = text.replace(/[^\d]/g, '');
    let minutes = parseInt(digits, 10);

    return minutes >= 6;
}