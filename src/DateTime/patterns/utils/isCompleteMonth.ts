export function isCompleteMonth(text: string):boolean {
    let digits = text.replace(/[^\d]/g, '');
    let month = parseInt(digits, 10);

    return month >= 2;
}