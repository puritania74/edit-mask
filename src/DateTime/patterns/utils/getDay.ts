export function getDay(value: number) :string {
    let text;

    if (value === null || typeof value === 'undefined') {
        text = '';
    } else {
        let day = new Date(value).getUTCDate();
        text = day.toString();
    }

    return text;
}