export function isCompleteDay(text: string) :boolean{

    let digits = text.replace(/[^\d]/g, '');
    let day = parseInt(digits, 10);

    return day > 3;
}