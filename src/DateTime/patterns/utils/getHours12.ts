export function getHours12(value: number) :string{

    let text :string;

    if (value === null || typeof value === 'undefined') {
        text = '';
    } else {
        let hours = new Date(value).getUTCHours();

        if (hours === 0) {
            text = '12'
        } else if (hours < 13) {
            text =  hours.toString();
        } else {
            text = (hours - 12).toString();
        }
        /*

        11:59 pm
        12:00 am    00:00
        12:01 am    00:01
        ...
        01:00 am    01:00
        ....
        11:59 am    11:59
        12:00 pm    12:00
        ....
        01:00 pm    13:00
         */
    }

    return text;
}