export function isCompleteHours(text:string):boolean {

    let digits = text.replace(/[^\d]/g, '');
    let hours = parseInt(digits, 10);

    return hours > 2;
}