export function getMonth(value: number) :string {
    let text:string;

    if (value === null || typeof value === 'undefined') {
        text = '';
    } else {
        let month = new Date(value).getUTCMonth() + 1;
        text = month.toString();
    }

    return text;
}