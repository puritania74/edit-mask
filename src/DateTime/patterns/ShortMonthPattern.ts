import { FloatWidthPattern } from "../../common/FloatWidthPattern";
import { isCompleteMonth } from "./utils/isCompleteMonth";
import { getMonth } from "./utils/getMonth";

export class ShortMonthPattern extends FloatWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'month', {min: 1, max: 12, minWidth: 1, maxWidth: 2});
    }

    getIsComplete(): boolean {
        return isCompleteMonth(this.getText());
    }

    applyValue( value: number ) {
        this.setText(getMonth(value));
    }

    getValue():number {
        let value = super.getValue();
        return value === null ? value : value - 1;
    }
}