import { FixedWidthPattern } from "../../common/FixedWidthPattern";
import { isCompleteMonth } from "./utils/isCompleteMonth";
import { getMonth } from "./utils/getMonth";

export class MonthPattern extends FixedWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'month', {min: 1, max: 12, width: 2});
    }

    getIsComplete(): boolean {
        return isCompleteMonth(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let text = getMonth(value);
            this.setText(this.formatDecimal(text));
        }
    }

    getValue():number {
        let value = super.getValue();
        return value === null ? value : value - 1;
    }
}