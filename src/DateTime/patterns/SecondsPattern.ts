import { FixedWidthPattern } from "../../common/FixedWidthPattern";
import { isCompleteSeconds } from "./utils/isCompleteSeconds";
export class SecondsPattern extends FixedWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'seconds', {min: 0, max: 59, width: 2});
    }

    getIsComplete(): boolean {
        return isCompleteSeconds(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let seconds = new Date(value).getUTCSeconds();
            this.setText(this.formatDecimal(seconds.toString()));
        }
    }

}