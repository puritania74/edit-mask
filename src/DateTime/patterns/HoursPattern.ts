import { FixedWidthPattern } from "../../common/FixedWidthPattern";
import { isCompleteHours } from "./utils/isCompleteHours";
import { getHours } from "./utils/getHours";

export class HoursPattern extends FixedWidthPattern<number> {

    constructor(locale, template:string) {
        super(locale, template, 'hours', {min: 0, max: 23, width: 2});
    }

    getIsComplete(): boolean {
        return isCompleteHours(this.getText());
    }

    applyValue( value: number ) {
        if (value === null || typeof value === 'undefined') {
            this.setText('');
        } else {
            let text = getHours(value);
            this.setText(this.formatDecimal(text));
        }
    }

}