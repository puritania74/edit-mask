import 'mocha';
import * as chat from 'chai';
import { Locale } from "../locales/Locale";
import { DateTimeMaskBuilder } from "./builders/DateTimeMaskBuilder";
import { DateTimeMask } from "./DateTimeMask";

let assert = chat.assert;

describe('DateTimeMask', function () {

    let builder = new DateTimeMaskBuilder();
    let locale = new Locale('ru');

    describe('should correctly format', () => {
        let mask: DateTimeMask;

        beforeEach(() => {
            mask = <DateTimeMask>builder.build('dd.MM.yyyy HH:mm:ss', locale);
        });

        it('empty value', () => {
            assert.equal(mask.getText(), '__.__.____ __:__:__');
        });

        it('date value', () => {
            mask.setValue(Date.UTC(2017, 3, 27, 13, 24, 0));
            assert.equal(mask.getText(), '27.04.2017 13:24:00');
        });

    });

    describe('should correctly input date', () => {

        let mask: DateTimeMask = <DateTimeMask>builder.build('dd.MM.yyyy', locale);

        let input = [
            {'2': '2_.__.____'},
            {'7': '27.__.____'},
            {'4': '27.04.____'},
            {'2': '27.04.2___'},
            {'0': '27.04.20__'},
            {'1': '27.04.201_'},
            {'7': '27.04.2017'},
        ];

        checkInputText(mask, input, Date.UTC(2017, 3, 27));

    });

    describe('should correctly input time', () => {

        let mask: DateTimeMask = <DateTimeMask>builder.build('HH:mm:ss', locale);

        let input = [
            {'1': '1_:__:__'},
            {'3': '13:__:__'},
            {'3': '13:3_:__'},
            {'8': '13:38:__'},
            {'0': '13:38:0_'},
            {'1': '13:38:01'}
        ];

        checkInputText(mask, input, Date.UTC(1970, 0, 1, 13, 38, 1));
    });

    function checkInputText(mask: DateTimeMask, input: Array<{[char:string]: string}| number>, value: number) {
        let position = 0;
        let lastValue = null;

        mask.onChangeValue((event) => {
            lastValue = event.newValue;
        });

        input.concat(value).forEach(data => {
            if (typeof data === 'number') {
                it(`valid value ${data}`, () => {
                    assert.equal(mask.getValue(), data);
                    assert.equal(lastValue, data);
                });

                it(`valid last value ${data} in onChangeValue()`, () => {
                    assert.equal(lastValue, data);
                });
                return;
            }
            let char = Object.keys(data).pop();
            let text = data[char];

            it(`${char} change text to ${text}`, () => {
                let input = mask.getText();
                input = input.substring(0, position) + char + input.substring(position);
                position = mask.setInput(input, position + 1);
                assert.equal(mask.getText(), text);
            });

        });


    }
});
