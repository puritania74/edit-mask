
import { MaskBuilder } from "../../core/builders/MaskBuilder";
import { DateTimeMask } from "../DateTimeMask";
import { Locale } from "../../locales/Locale";
import { PatternsBuilder } from "../../core/builders/PatternsBuilder";
import { buildStaticPattern } from "../../common/StaticPatternBuilder";
import { StringSelector } from "../../core/selectors/StringSelector";
import { YearPattern } from "../patterns/YearPattern";
import { DayPattern } from "../patterns/DayPattern";
import { MonthPattern } from "../patterns/MonthPattern";
import { HoursPattern } from "../patterns/HoursPattern";
import { MinutesPattern } from "../patterns/MinutesPattern";
import { SecondsPattern } from "../patterns/SecondsPattern";
import { ShortDayPattern } from "../patterns/ShortDayPattern";
import { ShortMonthPattern } from "../patterns/ShortMonthPattern";
import { FullMonthPattern } from "../patterns/FullMonthPattern";
import { MeridiemPattern } from "../patterns/MeridiemPattern";
import { Hours12Pattern } from "../patterns/Hours12Pattern";
import { ShortMinutesPattern } from "../patterns/ShortMinutesPattern";
import { ShortHours12Pattern } from "../patterns/ShortHours12Pattern";
import { ShortHoursPattern } from "../patterns/ShortHoursPatterm";
import { ShortSecondsPattern } from "../patterns/ShortSecondsPattern";

export class DateTimeMaskBuilder extends MaskBuilder<number>{

    protected initPatternsBuilder(): PatternsBuilder<any> {
        let patternsBuilder = new PatternsBuilder(buildStaticPattern);

        return patternsBuilder
            .registerPattern(new StringSelector('dd'), (template:string, locale:Locale) => new DayPattern(locale, template))
            .registerPattern(new StringSelector('d'), (template:string, locale:Locale) => new ShortDayPattern(locale, template))

            .registerPattern(new StringSelector('MMMM'), (template:string, locale:Locale) => new FullMonthPattern(locale, template))
            .registerPattern(new StringSelector('MM'), (template:string, locale:Locale) => new MonthPattern(locale, template))
            .registerPattern(new StringSelector('M'), (template:string, locale:Locale) => new ShortMonthPattern(locale, template))

            .registerPattern(new StringSelector('yyyy'), (template:string, locale:Locale) => new YearPattern(locale, template))

            .registerPattern(new StringSelector('HH'), (template:string, locale:Locale) => new HoursPattern(locale, template))
            .registerPattern(new StringSelector('H'), (template:string, locale:Locale) => new ShortHoursPattern(locale, template))
            .registerPattern(new StringSelector('hh'), (template:string, locale:Locale) => new Hours12Pattern(locale, template))
            .registerPattern(new StringSelector('h'), (template:string, locale:Locale) => new ShortHours12Pattern(locale, template))

            .registerPattern(new StringSelector('mm'), (template:string, locale:Locale) => new MinutesPattern(locale, template))
            .registerPattern(new StringSelector('m'), (template:string, locale:Locale) => new ShortMinutesPattern(locale, template))
            .registerPattern(new StringSelector('ss'), (template:string, locale:Locale) => new SecondsPattern(locale, template))
            .registerPattern(new StringSelector('s'), (template:string, locale:Locale) => new ShortSecondsPattern(locale, template))
            .registerPattern(new StringSelector('tt'), (template:string, locale:Locale) => new MeridiemPattern(locale, template));

    }


    protected createMask(locale: Locale): DateTimeMask {
        return new DateTimeMask(locale);
    }

}