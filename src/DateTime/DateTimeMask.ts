import { Mask } from "../core/Mask";
import { isEmpty } from "../utils/isEmpty";
import { OutOfRangeEvent } from "../core/OutOfRangeEvent";

export class DateTimeMask extends Mask<number> {

    protected timezoneOffset:number = 0;    //offset = UTC - local in minutes

    protected minValue: number;

    protected maxValue: number;

    setTimezoneOffset(offset: number){
        this.timezoneOffset = offset;
    }

    getTimezoneOffset(){
        return this.timezoneOffset;
    }

    setMinValue(minValue:number) {
        this.minValue = minValue;
    }

    getMinValue():number {
        return this.minValue;
    }

    setMaxValue(maxValue:number) {
        this.maxValue = maxValue;
    }

    getMaxValue():number {
        return this.maxValue;
    }

    protected buildValue(): number {

        let lastValue = this.lastValue || 0;
        let defaultDate = new Date(lastValue - this.getTimezoneOffset() * 60 * 1000);


        let data = this.patterns.getPatternsValue();

        let year = isEmpty(data.year) ? defaultDate.getUTCFullYear() : data.year;
        let month = isEmpty(data.month) ? defaultDate.getUTCMonth() : data.month;
        let day = isEmpty(data.day) ? defaultDate.getUTCDate() : data.day;
        let hours;
        let minutes = isEmpty(data.minutes) ? defaultDate.getUTCMinutes() : data.minutes;
        let seconds = isEmpty(data.seconds) ? defaultDate.getUTCSeconds() : data.seconds;
        let milliseconds = isEmpty(data.milliseconds) ? defaultDate.getUTCMilliseconds() : data.milliseconds;

        if (isEmpty(data.hours)) {
            hours = defaultDate.getUTCHours();
        } else if (!isEmpty(data.meridiem)) {
            //use AM/PM: AM - 0, PM - 1
            if (data.meridiem === 0 && data.hours === 12) {
                //12:00 a.m.
                hours = 0;
            } else if (data.meridiem === 1 && data.hours === 12) {
                //12:00 p.m
                hours = 12;
            } else if (data.meridiem === 1) {
                hours = data.hours + 12;
            } else {
                hours = data.hours;
            }
        } else {
            hours = data.hours;
        }

        let value = Date.UTC(year, month, day, hours, minutes, seconds, milliseconds);

        return value + this.getTimezoneOffset() * 60 * 1000;
    }

    applyValue(value: number) {
        if (value !== null && typeof value !== 'undefined') {
            value = value - this.getTimezoneOffset() * 60 * 1000;
        }

        super.applyValue(value);
    }

    validate(value: number):boolean {
        let min = this.getMinValue();
        let max = this.getMaxValue();
        let valid:boolean;

        if (isEmpty(value)) {
            valid = true;
        } else if (!isEmpty(min) && !isEmpty(max)) {
            valid = value >= min && value <= max;
        } else if (!isEmpty(min)) {
            valid = value >= min;
        } else if (!isEmpty(max)) {
            valid = value <= max;
        } else {
            //any value
            valid = true
        }

        if (!valid) {
            let event = new OutOfRangeEvent(value);
            this.eventEmitter.emit(OutOfRangeEvent.eventName, event);
        }

        return valid;
    }

}