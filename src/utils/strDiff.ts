export enum DiffOperation {
    Insert,
    Delete,
    Change,
    None
}


export interface IDiffResult {
    operation: DiffOperation
    position?: number
    src?: string
    dst?: string
}

export function strDiff( text1: string, text2: string, position?:number ): IDiffResult {

    let
        operation: DiffOperation = DiffOperation.None,
        result: IDiffResult = {operation};

    if (text1 === text2) {
        //no changes
        result = {
            operation: DiffOperation.None
        }
    } else if(text2.length === 0) {
        //delete all text
        result = {
            operation: DiffOperation.Delete,
            src: text1,
            position: 0
        }
    } else {
        let atBegin = getCommonAtBegin( text1, text2 );
        let atEnd = getCommonAtEnd( text1.substr( atBegin ), text2.substr( atBegin ) );
        let src, dst, position = atBegin;

        if( atBegin === 0 && atEnd === 0 ) {
            //Нет ничего общего
            operation = DiffOperation.Change;
            src = text1;
            dst = text2;

        } else if (atBegin + atEnd < text1.length) {
            operation = DiffOperation.Change;
            src = text1.substr( atBegin, text1.length - atBegin - atEnd );
            dst = text2.substr( atBegin, text2.length - atBegin - atEnd );
            if (dst.length === 0 && src.length > 0) {
                dst = void 0;
                operation = DiffOperation.Delete;
            }
        }else if(text1.length > text2.length ) {
            operation = DiffOperation.Delete;
            src = text1.substr( atBegin, text1.length - atBegin - atEnd );
        } else if( text1.length < text2.length ) {
            operation = DiffOperation.Insert;
            dst = text2.substr( atBegin, text2.length - atBegin - atEnd );
        } else {
            operation = DiffOperation.Change;
            src = text1.substr( atBegin, text1.length - atBegin - atEnd );
            dst = text2.substr( atBegin, text2.length - atBegin - atEnd );
        }

        result = {
            operation,
            src,
            dst,
            position
        }
    }

    return checkAmbiguousOperation(text1, text2, result, position);
}

/**
 * @description Разрешает неоднозначные ситуации, когда необходиммо учитывать позицию курсора напр "111" => "1111"
 */
function checkAmbiguousOperation(text1: string, text2: string, diff: IDiffResult, position: number):IDiffResult {
    switch(diff.operation) {
        case DiffOperation.Insert:
            diff = checkAmbiguousInsert(text1, text2, diff, position);
            break;
        case DiffOperation.Delete:
            diff = checkAmbiguousDelete(text1, text2, diff, position);
            break;
    }

    return diff;
}

function checkAmbiguousInsert(text1: string, text2: string, diff: IDiffResult, position: number):IDiffResult {
    let updatedPosition = null;

    for (let i = diff.position; i >= 0; i = i - diff.dst.length) {
        let checkPosition = i - diff.dst.length;
        if (text2.substring(checkPosition, i) === diff.dst) {
            if (checkPosition === position - diff.dst.length) {
                updatedPosition = checkPosition;
                break;
            }
        }
    }

    if (updatedPosition !== null) {
        diff.position = updatedPosition;
    }

    return diff;
}

function checkAmbiguousDelete(text1: string, text2: string, diff: IDiffResult, position: number):IDiffResult {
    let updatedPosition = null;

    for (let i = diff.position; i >= 0; i = i - diff.src.length) {
        let checkPosition = i - diff.src.length;
        if (text1.substring(checkPosition, i) === diff.src) {
            if (checkPosition >= position) {
                updatedPosition = checkPosition
            } else {
                break;
            }
        }
    }

    if (updatedPosition !== null) {
        diff.position = updatedPosition;
    }

    return diff;
}

/**
 * @param text1
 * @param text2
 * @returns {number} Количество совпадающих символов в начале строки
 */
function getCommonAtBegin( text1:string, text2:string ):number {
    let len = Math.min( text1.length, text2.length );
    let common = 0;

    for( let i = 0; i < len; i = i + 1 ) {
        if( text1[ i ] !== text2[ i ] ) {
            break;
        }
        common++;
    }

    return common;
}

/**
 *
 * @param text1
 * @param text2
 * @returns {number} Количество совпадающих символов в конце строки
 */
function getCommonAtEnd( text1:string, text2:string ):number {
    let arr1 = text1.split( '' ).reverse();
    let arr2 = text2.split( '' ).reverse();
    let common = 0;
    let len = Math.min( arr1.length, arr2.length );

    for( let i = 0; i < len; i = i + 1 ) {
        if( arr1[ i ] !== arr2[ i ] ) {
            break;
        }
        common++;
    }
    return common;
}