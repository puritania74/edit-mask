import 'mocha';
import * as chat from 'chai';
import { padEnd } from "./padEnd";
let assert = chat.assert;

describe('padEnd', function () {

    it('should correctly format', () => {
        assert.equal(padEnd('', 2, '_'), '__');
        assert.equal(padEnd('1', 2, '_'), '1_');
        assert.equal(padEnd('12', 2, '_'), '12');
        assert.equal(padEnd('123', 2, '_'), '123');
    })
});
