/***
 * @see https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
 */

export function padStart( text: string, length: number, pad: string ): string {
    pad = pad || ' ';
    if( text.length > length ) {
        return text;
    } else {
        length = length - text.length;
        if( length > pad.length ) {
            pad += pad.repeat( length / pad.length );
        }
        return pad.slice( 0, length ) + text;
    }
}