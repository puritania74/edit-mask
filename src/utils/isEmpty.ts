export function isEmpty(value: any):boolean {
    return typeof value === 'undefined' || value === null;
}