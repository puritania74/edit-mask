import 'mocha';
import * as chat from 'chai';
import { strDiff, DiffOperation } from "./strDiff";
let assert = chat.assert;

describe('strDiff', function () {

    it('should return no change', function(  ) {
        let res = strDiff('123', '123');

        assert.equal(res.operation, DiffOperation.None);
    });

    it('insert at begin', function(  ) {
        let res = strDiff('__.__.____', '12__.__.____');
        assert.equal(res.operation, DiffOperation.Insert);
        assert.equal(res.position, 0);
        assert.isUndefined(res.src);
        assert.equal(res.dst, '12');
    });

    it('partial change text', function(  ) {
        let res = strDiff('__.__.____', '__.05.____');
        assert.equal(res.operation, DiffOperation.Change);
        assert.equal(res.position, 3);
        assert.equal(res.src, '__');
        assert.equal(res.dst, '05');
    });

    it('change entire text', function(  ) {
        let res = strDiff('__.__.____', '14.03.2017');
        assert.equal(res.operation, DiffOperation.Change);
        assert.equal(res.position, 0);
        assert.equal(res.src, '__.__.____');
        assert.equal(res.dst, '14.03.2017');
    });

    it('insert text', function(  ) {
        let res = strDiff('__._.____', '__.12_.____');
        assert.equal(res.operation, DiffOperation.Insert);
        assert.equal(res.position, 3);
        assert.isUndefined(res.src);
        assert.equal(res.dst, '12');
    });

    it('delete all', function(  ) {
        let res = strDiff('2017', '');
        assert.equal(res.operation, DiffOperation.Delete);
        assert.equal(res.position, 0);
        assert.equal(res.src, '2017');
        assert.isUndefined(res.dst);
    })

    it('change text', function(  ) {
        let res = strDiff('23/10', '9/10');
        assert.equal(res.operation, DiffOperation.Change);
        assert.equal(res.position, 0);
        assert.equal(res.src, '23');
        assert.equal(res.dst, '9');
    });

    it('delete char from text', function(  ) {
        let res = strDiff('__/__/____ __:__', '__/______ __:__');
        assert.equal(res.operation, DiffOperation.Delete);
        assert.equal(res.position, 5);
        assert.equal(res.src, '/');
        assert.isUndefined(res.dst);
    });





})