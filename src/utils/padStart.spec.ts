import 'mocha';
import * as chat from 'chai';
import { padStart } from "./padStart";

let assert = chat.assert;

describe('padStart', function () {

    it('should correctly format', () => {
        assert.equal(padStart('', 2, '0'), '00');
        assert.equal(padStart('1', 2, '0'), '01');
        assert.equal(padStart('12', 2, '0'), '12');
        assert.equal(padStart('123', 2, '0'), '123');
    })
});
