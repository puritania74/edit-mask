export function restorePosition( before: string, after: string, position: number ): number {

    return before.substr( 0, position )
        .split( '' )
        .reduce( ( offset, char ) => {
            let i = after.indexOf( char, offset );
            return (i !== -1) ? i + 1 : offset;
        }, 0 );

}