import 'mocha';
import * as chat from 'chai';
import { restoreNumberPrecision } from "./restoreNumberPrecision";

let assert = chat.assert;

describe('restoreNumberPrecision', function () {

    it('should restore number precision', () => {
        assert.equal(restoreNumberPrecision(1.1, 1.1 - 1), 0.1);
        assert.equal(restoreNumberPrecision(1.1, 0.599), 0.6);
        assert.equal(restoreNumberPrecision(1, 2.99999), 3);
    });

});
