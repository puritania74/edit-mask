export function restoreNumberPrecision(value1: number, value2: number): number {
    let [, fract = ''] = value1.toString(10).split('.');
    return +value2.toFixed(fract.length);
}