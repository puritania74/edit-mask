import { Mask } from "./core/Mask";

export abstract class Editor<T> implements EventListenerObject{

    constructor(protected el: HTMLInputElement,
                protected mask: Mask<T>) {
        this.subscribeToElementEvents();
        this.updateInputElement();
    }

    getValue():T {
        return this.mask.getValue();
    }

    setValue(value: T) {
        this.mask.setValue(value);
        this.updateInputElement()
    }

    onChangeValue(handler){
        this.mask.onChangeValue(handler);
    }

    onInvalidValue(handler){
        this.mask.onInvalidValue(handler);
    }

    protected setCursorPosition(start:number, end?:number) {
        end = end || start;
        this.el.setSelectionRange(start, end);
        //Fix for Android

        setTimeout(() => {
            if (this.el) {
                this.el.setSelectionRange(start, end);
            }
        }, 4);
    }

    protected selectAll() {
        this.el.setSelectionRange(0, this.el.value.length);
    }

    protected subscribeToElementEvents() {
        this.el.addEventListener('keydown', this );
        this.el.addEventListener('input', this);
        this.el.addEventListener('focus', this);
        this.el.addEventListener('blur', this);
    }

    protected unsubscribeToElementEvents() {
        this.el.removeEventListener('keydown', this );
        this.el.removeEventListener('input', this);
        this.el.removeEventListener('focus', this);
        this.el.removeEventListener('blur', this);
    }

    protected destroy() {
        this.unsubscribeToElementEvents();
        this.mask.dispose();
        this.mask = null;
        this.el = null;
    }

    handleEvent(event:Event) {
        switch(event.type) {
            case 'keydown':
                this.onKeydownHandler(event);
                break;
            case 'input':
                this.onInputHandler();
                break;
            case 'focus':
                this.onFocusHandler();
                break;
            case 'blur':
                this.onBlurHandler();
                break;
        }
    }

    protected onBlurHandler() {
        this.mask.updateDisplay();
        this.updateInputElement();
    }

    protected onFocusHandler() {
        if (this.mask.getIsEmpty()) {
            let position = this.el.selectionStart;
            let pos = this.mask.getInputPosition(position);
            this.updateInputElement(pos);
        }
    }

    protected onInputHandler() {
        let position = this.el.selectionStart;
        let text = this.el.value;

        let newPosition = this.mask.setInput(text, position);
        this.updateInputElement(newPosition);
    }

    protected onKeydownHandler(event) {
        if (event.altKey || event.ctrlKey || event.shiftKey) {
            return;
        }

        let position = this.el.selectionStart;
        let selection;

        switch(event.key) {
            case 'ArrowUp':
                event.preventDefault();
                selection = this.mask.setNextPatternValue(position);
                if (selection) {
                    this.updateInputElement(selection.start, selection.end);
                }
                break;
            case 'ArrowDown':
                event.preventDefault();
                selection = this.mask.setPrevPatternValue(position);
                if (selection) {
                    this.updateInputElement(selection.start, selection.end);
                }
                break;
        }
    }

    protected updateInputElement(start?: number, end?: number) {
        this.el.value = this.mask.getText();

        if (typeof start !== 'undefined' && start !== null) {
            end = end || start;

            if (this.mask.getInvalid()) {
                this.selectAll();
            } else {
                this.setCursorPosition(start, end);
            }
        } else if (this.mask.getInvalid()) {
            this.selectAll();
        }

    }
}