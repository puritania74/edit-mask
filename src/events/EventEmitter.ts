import deleteProperty = Reflect.deleteProperty;
export interface IEventData {

}

export interface IEventHandler {
    (eventName: string, data: IEventData): void
}

export interface IRegisteredEventHandlers {
    [name:string]: Array<IEventHandler>
}

export class EventEmitter {

    private handlers:IRegisteredEventHandlers = {};

    on(eventName: string, eventHandler: IEventHandler): () => void {
        let handlers = this.handlers[eventName];

        if (!handlers) {
            this.handlers[eventName] = handlers = [];
        }

        if (handlers.indexOf(eventHandler) === -1) {
            handlers.push(eventHandler);
        }

        return () => {
            //unsubscribe
            let index = handlers.indexOf(eventHandler);
            if (index !== -1) {
                handlers.splice(index, 1);
            }
        }
    }

    dispose() {
        Object.keys(this.handlers).forEach(name => delete this.handlers[name] );
    }

    emit(eventName: string, data: IEventData):void {
        let handlers = this.handlers[eventName];

        if (Array.isArray(handlers)) {
            handlers.forEach(handler => handler.call(null, eventName, data));
        }
    }
}