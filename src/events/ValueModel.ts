import { EventEmitter, IEventData, IEventHandler } from "./EventEmitter";

const CHANGE_EVENT = 'change';

export interface IOnChangeEventData<T> extends IEventData {
    oldValue: T
    newValue: T
    params: {[name:string]: any}
}

export interface IOnChangeEventHandler<T> extends IEventHandler {
    ( eventName: string, data: IOnChangeEventData<T> )
}

export class ValueModel<T> {

    protected value: T;

    protected eventEmitter = new EventEmitter();

    setValue( value: T, params?: {[name:string]: any}) {

        let changed = this.value !== value;

        if (typeof value === 'number' && typeof this.value === 'number') {
            if (isNaN(value) && isNaN(this.value)) {
                changed = false;
            }
        }

        let oldValue = this.value;
        this.value = value;

        if( changed ) {
            let data: IOnChangeEventData<T> = {
                oldValue: oldValue,
                newValue: value,
                params: params || {}
            };
            this.eventEmitter.emit( CHANGE_EVENT, data );
        }
    }

    getValue(): T {
        return this.value;
    }

    onChange( handler: IOnChangeEventHandler<T> ): Function {
        return this.eventEmitter.on( CHANGE_EVENT, handler );
    }

    dispose() {
        this.eventEmitter.dispose();
        this.value = null;
    }
}