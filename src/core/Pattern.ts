import { Locale } from "../locales/Locale";
const BLANK_CHAR = '_';

export interface ISetInputCallback {
    (restText:string, applyText: string): void
}

export abstract class Pattern<T> {

    protected text: string;

    protected isActive: boolean = false;

    protected isEditable: boolean = true;

    protected blank: string = BLANK_CHAR;

    constructor(protected locale:Locale,
                protected template:string,
                protected name?: string) {

    }

    setBlank(blank: string) {
        this.blank = blank || BLANK_CHAR;
    }

    getBlank():string {
        return this.blank;
    }

    getName() :string {
        return this.name;
    }

    canActivate(): boolean {
        return true;
    }

    setIsActive(isActive: boolean) {
        if (!this.canActivate()) {
            isActive = false;
        }

        this.isActive = isActive;
    }

    getNextInput(): string {
        return this.getText();
    }

    getPrevInput(): string {
        return this.getText();
    }

    abstract getIsEmpty():boolean

    abstract getIsComplete():boolean;

    abstract getIsValid():boolean;

    // /**
    //  * @desc Возращает признак готовности шаблона к конвертации в значение
    //  */
    // abstract getIsReady():boolean;

    abstract applyValue(value: T)

    getText() :string {
        if(this.text === null || typeof this.text === 'undefined') {
            this.updateText()
        }

        return this.text;
    }


    canInputChar(char:string):boolean {

        return true;
    }

    protected setText(text:string) {
        let changed = this.isEqualText(text, this.text);

        this.text = text;

        return changed;
    }

    protected isEqualText(text1:string, text2:string): boolean {
        return text1 === text2;
    }

    protected trimBlankLeft(text:string) :string {
        let blank = this.getBlank();

        while(text.indexOf(blank) === 0) {
            text = text.substring(blank.length);
        }
        return text
    }

    protected trimBlankRight(text:string) :string {
        let blank = this.getBlank();

        while(text.lastIndexOf(blank) ===  text.length - 1) {
            text = text.substring(0, blank.length - 1);
        }
        return text
    }

    protected cleanBlank(text:string) :string {
        let blank = this.getBlank();

        while(text.indexOf(blank) !== -1) {
            text = text.replace(blank, '');
        }
        return text
    }

    protected trimBlank(text:string):string {
        let blank = this.getBlank();

        while(text.substr(0, 1) === blank) {
            text = text.substr(1);
        }

        while(text.substr(text.length - 1) === blank) {
            text = text.substr(0, text.length - 1);
        }

        return text;
    }

    protected finalizeInput():boolean {
        return false;
    }

    abstract updateText()

    abstract setInput(input: string, cb? : ISetInputCallback): string

    getValue():any {
        return void 0;
    }


}