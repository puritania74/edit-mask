import { PatternsList } from "./PatternsList";
import { Pattern } from "./Pattern";
import { ITextSelection } from "./ITextSelection";
import { Locale } from "../locales/Locale";
import { IOnChangeEventData, ValueModel } from "../events/ValueModel";
import { restorePosition } from "../utils/restorePosition";
import { EventEmitter } from "../events/EventEmitter";
import { OutOfRangeEvent } from "./OutOfRangeEvent";


export abstract class Mask<T> {

    protected patterns: PatternsList<T> = new PatternsList<T>();

    protected lastValue:T;

    protected invalid: boolean = false;

    protected value:ValueModel<T> = new ValueModel<T>();

    protected eventEmitter: EventEmitter = new EventEmitter();

    constructor(protected locale:Locale) {
        this.value.onChange((eventName, eventData) => this.onChangeValueHandler(eventData))
    }

    setPatterns(list: Array<Pattern<T>>) {
        this.patterns.setList(list);
    }

    getInvalid():boolean {
        return this.invalid;
    }

    getIsEmpty():boolean {
        return this.patterns.getIsEmpty();
    }

    onChangeValue(handler):Function {
        return this.value.onChange((eventName, eventData) => handler.call(null, eventData));
    }


    onInvalidValue(cb) {
        return this.eventEmitter.on(OutOfRangeEvent.eventName, cb);
    }

    protected onChangeValueHandler(eventData:IOnChangeEventData<T>) {
        this.updateInvalid();
        if (typeof eventData.newValue === 'undefined') {
            //skip undefined for raw input
            return;
        }
        this.lastValue = eventData.newValue;
        if (eventData.params.update) {
            this.applyValue(eventData.newValue);
        }

    }

    applyValue(value: T) {
        this.patterns.getList().map((pattern, index) => pattern.applyValue(value));
        this.patterns.updateText();
    }


    getText():string {
        return this.getInvalid() ? this.getInvalidText() : this.patterns.getText();
    }

    setNextPatternValue(position: number):ITextSelection {
        let res = this.patterns.setNextPatternValue(position);
        this.updateValue();

        return res;
    }

    setPrevPatternValue(position: number): ITextSelection {
        let res = this.patterns.setPrevPatternValue(position);
        this.updateValue();

        return res;
    }

    setInput(input: string, position?: number):number {
        let newPosition = this.patterns.setInput(input, position);

        let currentText = this.getText();
        this.patterns.updateText();
        let res = this.restorePosition(currentText, newPosition);

        this.updateValue();

        return res;
    }

    setValue(value: T) {
        this.value.setValue(value, {update: true});
    }

    getValue():T {
        return this.value.getValue();
    }

    getInputPosition(position: number) :number {
        return this.patterns.getInputPosition(position);
    }

    /**
     * @returns cursor position or null if not handled
     */
    handleKey(key:string, position: number):number|null {
        return null;
    }

    restorePosition(text:string, position: number):number {
        return restorePosition(text, this.getText(), position);
    }

    updateDisplay():Mask<T> {
        this.patterns.updateDisplay();
        return this;
    }

    protected updateValue(){
        let isValid = this.patterns.getIsValid();

        if (isValid) {
            //build value from current value and patterns
            let value = this.buildValue();
            if (this.validate(value)) {
                this.value.setValue(this.buildValue());
            }
        } else {
            //setup undefined value
            this.value.setValue(void 0);
        }
    }

    protected updateInvalid() {

    }

    protected getInvalidText():string {
        return 'Error';
    }

    protected setInvalid(invalid: boolean) :void {
        this.invalid = invalid;
    }

    protected validate(value: T):boolean {
        return true;
    }

    protected abstract buildValue():T

    dispose() {
        this.patterns.dispose();
        this.lastValue = null;
        this.value.dispose();
        this.eventEmitter.dispose();
    }
}