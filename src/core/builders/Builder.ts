import { MaskBuilder } from "./MaskBuilder";
import { Mask } from "../Mask";
import { Locale } from "locales/Locale";

interface IMaskBuildersRegistry {
    [name: string]: MaskBuilder<any>
}

export class Builder {

    protected builders:IMaskBuildersRegistry = {};

    constructor() {

    }

    build(name:string, template: string, locale:Locale): Mask<any> {
        let maskBuilder = this.builders[name];

        return maskBuilder.build(template, locale);
    }

    register(name: string, builder: MaskBuilder<any>) {
        this.builders[name] = builder;
    }
}