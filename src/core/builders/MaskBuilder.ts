import { PatternsBuilder } from "./PatternsBuilder";
import { Mask } from "core/Mask";
import { Locale } from "../../locales/Locale";

export abstract class MaskBuilder<T> {

    protected patternsBuilder: PatternsBuilder<T>;

    constructor() {
        this.patternsBuilder = this.initPatternsBuilder();
    }

    build(template:string, locale:Locale):Mask<any> {

        let mask = this.createMask(locale);
        let patterns = this.patternsBuilder.build(template, locale);
        mask.setPatterns(patterns);
        return mask;
    }

    protected abstract initPatternsBuilder():PatternsBuilder<T>

    protected abstract createMask(locale:Locale):Mask<any>

}