import { Pattern } from "../Pattern";
import { Locale } from "../../locales/Locale";
import { ISelector } from "../selectors/ISelector";

export interface IPatternBuilder {
    (template: string, locale: Locale): Pattern<any>
}

export interface IRegisteredPattern {
    patternSelector: ISelector
    patternBuilder: IPatternBuilder
}

export class PatternsBuilder<T> {

    protected registeredPatterns: Array<IRegisteredPattern> = [];

    constructor(protected defaultPatternBuilder:IPatternBuilder) {

    }

    registerPattern(patternSelector: ISelector, patternBuilder: IPatternBuilder):PatternsBuilder<T> {
        this.registeredPatterns.push({patternSelector, patternBuilder});

        return this;
    }

    build (template: string, locale:Locale): Array<Pattern<T>> {
        let patterns:Array<any> = [template];

        this.registeredPatterns.forEach((registered: IRegisteredPattern) => {
            let i = 0;
            do {
                let test = patterns[i];
                if(typeof test === 'string' && test.length > 0) {

                    let info =  registered.patternSelector.match(test);

                    if (info !== null) {
                        let parts = [
                            test.substring(0, info.start),
                            registered.patternBuilder(info.text, locale),
                            test.substr(info.end)
                        ];

                        patterns.splice(i, 1, ...parts);
                        i++;
                    }
                }
                i++;
            } while (i < patterns.length);
        });


        if (this.defaultPatternBuilder) {
            patterns.forEach((test, i) => {
                if (typeof test === 'string' && test.length > 0) {
                    patterns[i] = this.defaultPatternBuilder(test, locale);
                }
            });
        }

        patterns = patterns.filter(data => typeof data !== 'string');

        return patterns;
    }
}