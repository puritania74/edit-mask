import { ISelector, ISelectorMatch } from "./ISelector";

export class RegExpSelector implements ISelector {

    constructor(protected regexp:RegExp) {

    }

    match( text: string ): ISelectorMatch {
        let
            data:ISelectorMatch = null,
            patternText,
            patternStart,
            match = text.match(this.regexp);


        if (match) {
            patternText = match[0];
            patternStart = text.indexOf(patternText);
            data = {
                text: patternText,
                start: patternStart,
                end: patternStart + patternText.length
            }
        }

        return data;
    }

}