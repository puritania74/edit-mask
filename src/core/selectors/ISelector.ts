export interface ISelectorMatch {
    text: string
    start:number
    end: number
}

export interface ISelector{
    match(text: string):ISelectorMatch
}