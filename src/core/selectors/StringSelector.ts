import { ISelector, ISelectorMatch } from "./ISelector";

export class StringSelector implements ISelector {

    constructor(protected text:string) {

    }
    match( text: string ): ISelectorMatch {
        let
            match:ISelectorMatch = null,
            start = text.indexOf(this.text);

        if (start !== -1) {
            match = {
                text: this.text,
                start,
                end: start + this.text.length
            }
        }

        return match;
    }

}