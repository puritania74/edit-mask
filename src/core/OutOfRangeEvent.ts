import { IEventData } from "../events/EventEmitter";
const OUT_OF_RANGE_EVENT = 'OutOfRangeEvent';

export class OutOfRangeEvent<T>  implements IEventData {

    static eventName:string =  OUT_OF_RANGE_EVENT;

    constructor(public value:T) {

    }

}