import { Pattern } from "./Pattern";
import { strDiff, DiffOperation } from "../utils/strDiff";
import { ITextSelection } from "./ITextSelection";
import { restorePosition } from "../utils/restorePosition";
import { Mask } from "./Mask";


export class PatternsList<T> {

    protected list: Array<Pattern<T>> = [];

    constructor() {

    }

    setList(list: Array<Pattern<T>>){
        this.list = list.slice();
    }

    getList():Array<Pattern<T>> {
        return this.list;
    }

    getIsComplete():boolean {
        return this.list.every(pattern => pattern.getIsComplete());
    }

    getIsValid():boolean {
        return this.list.every(pattern => pattern.getIsValid());
    }

    getIsEmpty():boolean {
        return this.list.every(pattern => pattern.getIsEmpty());
    }

    setInput(input: string, position?: number):number {
        let text = this.getText();

        let textDiff = strDiff(text, input, position);
        let newPosition = position || 0;
        let updatePosition = true;

        //Ввод значений
        switch(textDiff.operation) {
            case DiffOperation.Insert:
                if (textDiff.dst.length === 1 && this.isSeparator(textDiff.dst, textDiff.position)) {
                    newPosition = this.getNextPatternOffset(position);
                    updatePosition = false;
                } else {
                    newPosition = this.inputInsert(textDiff.position, textDiff.dst);
                }
                break;
            case DiffOperation.Change:
                newPosition = this.inputChange(textDiff.position, textDiff.src, textDiff.dst);
                break;
            case DiffOperation.Delete:
                newPosition = this.inputDelete(textDiff.position, textDiff.src);
                break;
        }

        return newPosition;
    }

    isSeparator(text:string, position: number):boolean {
        let isSeparator = false;
        let pattern = this.getPatternByPositionLeft(position);
        let index = this.list.indexOf(pattern);
        let nextPattern = this.list[index + 1];

        if (nextPattern && nextPattern.canActivate() === false) {
            isSeparator = text === nextPattern.getText();
        }

        return isSeparator;
    }

    getNextPatternOffset(position: number) : number{
        let newPosition = position;
        let pattern = this.getPatternByPositionLeft(position);
        let nextPattern = this.getNextPattern(pattern);
        if (nextPattern) {
            newPosition = this.getPatternOffset(nextPattern);
        }

        return newPosition;
    }

    getNextPattern(pattern: Pattern<T>):Pattern<T> {
        let index = this.list.indexOf(pattern);

        return this.list
            .slice(index + 1)
            .filter(pattern => pattern.canActivate())[0];
    }

    inputInsert(position:number, insert: string):number {

        // let pattern = this.getPatternByPositionRight(position);
        let pattern = this.getInputPatternByPosition(position);

        if (!pattern) {
            return position;
        }

        let patternText = pattern.getText();
        //Новый текст с начала шаблона и до конца вставляемого текста
        let patternOffset = this.getPatternOffset(pattern);
        let before = patternText.substring(0 , position - patternOffset) ;
        let after = patternText.substring(position - patternOffset);
        let newPatternText = before + insert + after;


        if(insert.length === 1 && position >= patternOffset && position < patternOffset + patternText.length) {
            //try insert char in current pattern
            if (!pattern.canInputChar(insert)) {
                return position;
            }
        }

        let text = newPatternText;
        let usedText = '';
        let lastUsedPattern;
        for (let i = this.list.indexOf(pattern); i < this.list.length; i++) {
            let currentPattern = this.list[i];
            if (currentPattern !== pattern && !currentPattern.getIsEmpty()) {
                //В непустой шаблон тест не переносим
                break;
            }

            currentPattern.setInput(text, (restText, applyText) => {
                text = restText;
                if (applyText && applyText.length) {
                    usedText += applyText;
                    lastUsedPattern = currentPattern;
                }
            });

            if (text.length === 0) {
                break;
            }
        }

        if (lastUsedPattern) {
            this.setActive(lastUsedPattern);
            //Установить курсор в конец добавленногго фрагмента

            let usedLeft = '';
            let left = before + insert;

            if (lastUsedPattern.finalizeInput()) {
                if (pattern === lastUsedPattern) {
                    usedText = left = lastUsedPattern.getText();
                }
            }

            let displayText = this.list
                .slice(this.list.indexOf(pattern), this.list.indexOf(lastUsedPattern) + 1)
                .map(pattern => pattern.getText())
                .join('');

            for(let i = 0, start = 0; i < left.length; i++) {
                let char = left.substr(i, 1);
                let index = usedText.indexOf(char, start);
                if (index !== -1) {
                    usedLeft +=  char;
                    start = index + 1;
                }
            }

            position = patternOffset + usedLeft.split('').reduce((offset, char) => {
                let i = displayText.indexOf(char, offset);
                return (i !== -1) ? i + 1: offset;
            }, 0);

            if (lastUsedPattern.getIsComplete()) {
                let offset = this.getPatternOffset(lastUsedPattern);
                let text = lastUsedPattern.getText();
                if (offset + text.length === position) {
                    position = this.forwardPosition(position);
                }
            }

        }

        return position;
    }

    forwardPosition(position: number): number {
        let patternLeft = this.getPatternByPositionLeft(position);
        let patternRight = this.getPatternByPositionRight(position);
        let forwardPosition: number = position;


        if (patternLeft !== patternRight && !patternRight.canActivate()) {
            let index = this.list.indexOf(patternRight);
            let pattern;
            for (let i = index + 1; i < this.list.length; i++) {
                if (this.list[i].canActivate()) {
                    pattern = this.list[i];
                    break;
                }
            }

            if (pattern) {
                forwardPosition = this.getPatternOffset(pattern);
            }
        }

        return forwardPosition;
    }

    inputChange(position: number, from: string, to: string):number {

        let pattern1 = this.getPatternByPositionRight(position);
        let pattern2 = this.getPatternByPositionLeft(position + from.length);

        let patternText = pattern1.getText();
        let patternOffset = this.getPatternOffset(pattern1);
        let before = patternText.substring(0, position - patternOffset);
        let after = patternText.substring(position - patternOffset + from.length);
        let newPatternText = before + to + after;
        let lastUsedPattern;
        let usedText = '';

        if (pattern1 === pattern2) {    //Изменение в пределах одного шаблона
            lastUsedPattern = pattern1;
            //Если изменения в пределах одного шаблона - лишний текст в соседний шаблон не переносим!
            pattern1.setInput(newPatternText, (restText, applyText) => {
                if (applyText && applyText.length) {
                    usedText = applyText;
                }
            });
            if (pattern1.getIsEmpty()) {
                position = patternOffset;
            }
        } else {    //Изменения в нескольких шаблонах

            let index1 = this.list.indexOf(pattern1);
            let index2 = this.list.indexOf(pattern2);

            let text = newPatternText;

            //Изменения за границы изменяемых шаблонов не переносим!
            for (let i = index1; i <= index2; i++) {
                let currentPattern = this.list[i];

                currentPattern.setInput(text, (restText, applyText) => {
                    text = restText;
                    if (applyText && applyText.length) {
                        usedText += applyText;
                        lastUsedPattern = currentPattern;
                    }
                });
            }
        }

        if (lastUsedPattern) {
            this.setActive(lastUsedPattern);
            //Установить курсор в конец добавленногго фрагмента
            let displayText = this.list
                .slice(this.list.indexOf(pattern1), this.list.indexOf(lastUsedPattern) + 1)
                .map(pattern => pattern.getText())
                .join('');


            let usedLeft = '';
            let left = before + to;

            for(let i = 0, start = 0; i < left.length; i++) {
                let char = left.substr(i, 1);
                let index = usedText.indexOf(char, start);
                if (index !== -1) {
                    usedLeft +=  char;
                    start = index + 1;
                }
            }

            position = patternOffset + usedLeft.split('').reduce((offset, char) => {
                    let i = displayText.indexOf(char, offset);
                    return (i !== -1) ? i + 1: offset;
                }, 0);

        }

        return position;
    }

    inputDelete(position: number, deleted: string):number {

        let pattern1 = this.getPatternByPositionRight(position);
        let pattern2 = this.getPatternByPositionLeft(position + deleted.length);

        let index1 = this.list.indexOf(pattern1);
        let index2 = this.list.indexOf(pattern2);

        if (pattern1 === pattern2) {//Удаление в пределах одного шаблона
            let patternText = pattern1.getText();
            let patternOffset = this.getPatternOffset(pattern1);
            let before = patternText.substring(0, position - patternOffset);
            let after = patternText.substring(position - patternOffset + deleted.length);
            this.setActive(pattern1);

            let usedText:string = '';
            pattern1.setInput(before + after, (restText, applyText) => {
                usedText = applyText;
            });

            if (pattern1.canActivate() && pattern1.getIsEmpty()) {
                position = patternOffset;
            } else {
                position = patternOffset + restorePosition(patternText, usedText, position - patternOffset);
            }


        } else {//Удаление в нескольких шаблонах

            //Изменение правого шаблона
            let patternText = pattern2.getText();
            let patternOffset = this.getPatternOffset(pattern2);
            let newText = patternText.substring(position - patternOffset + deleted.length);
            pattern2.setInput(newText);

            //Изменение левого шаблона
            patternText = pattern1.getText();
            patternOffset = this.getPatternOffset(pattern1);
            newText = patternText.substring(0, position - patternOffset);
            pattern1.setInput(newText);

            //Очищаем промежуточные шаблоны
            this.list.slice(index1 + 1, index2).forEach(pattern => pattern.setInput(''));

            if (pattern1.canActivate() && pattern1.getIsEmpty()) {
                position = patternOffset;
            }
            this.setActive(pattern1);
        }

        //Установка курсора в позицию удаления
        return position;
    }

    setNextPatternValue(position:number) :ITextSelection {
        let selection:ITextSelection = null;
        let pattern = this.getPatternByPositionRight(position);

        if (pattern.canActivate()) {
            let input = pattern.getNextInput();

            pattern.setInput(input);
            this.clearActive();
            this.updateText();

            let patternOffset = this.getPatternOffset(pattern);
            selection = {
                start: patternOffset,
                end: patternOffset + pattern.getText().length
            }
        }

        return selection
    }

    setPrevPatternValue(position:number) :ITextSelection {
        let selection:ITextSelection = null;
        let pattern = this.getPatternByPositionRight(position);

        if (pattern.canActivate()) {
            let input = pattern.getPrevInput();

            pattern.setInput(input);
            this.clearActive();
            this.updateText();

            let patternOffset = this.getPatternOffset(pattern);
            selection = {
                start: patternOffset,
                end: patternOffset + pattern.getText().length
            }
        }

        return selection;

    }

    getInputPosition(position: number) :number {

        let pattern = this.getInputPatternByPosition(position);
        let inputPosition:number;

        let index = this.list.indexOf(pattern);
        let patterns = this.list.slice(0, index + 1).filter(pattern => pattern.canActivate()).reverse();

        let input;

        for(let i = 0; i < patterns.length; i = i + 1) {
            if (!patterns[i].getIsEmpty()) {
                input = patterns[i];
                break;
            }
        }

        if (input) {
            let patternOffset = this.getPatternOffset(input);
            let patternText = input.getText();
            if (position > patternOffset + patternText.length) {
                inputPosition = patternOffset + patternText.length;
            } else if (position < patternOffset) {
                inputPosition = patternOffset
            } else {
                inputPosition = position;
            }
        } else {
            input = patterns.pop();
            inputPosition = this.getPatternOffset(input);
        }


        return inputPosition;
    }

    clearActive() {
        this.list.forEach(p => p.setIsActive(false));
    }

    setActive(pattern: Pattern<T>) {
        this.list.forEach(p => p.setIsActive(p === pattern));
    }

    getText(): string {
        return this.list.map(pattern => pattern.getText()).join('');
    }

    getPatternByPosition(position: number):Pattern<T> {
        let map:Array<Pattern<T>> = [];

        this.list.forEach(pattern => {
            let text = pattern.getText();
            text.split('').forEach(char => map.push(pattern));
        });

        return map[position];
    }


    private getPatternsByPosition(position: number):Array<Pattern<T>> {
        let map:Array<Pattern<T>> = [];

        this.list.forEach(pattern => {
            let text = pattern.getText();
            text.split('').forEach(char => map.push(pattern));
        });

        let leftPattern = map.slice(0, position).filter(pattern => pattern !== null).pop();
        let rightPattern = map.slice(position).filter(pattern => pattern !== null).reverse().pop();

        return [leftPattern, rightPattern];
    }

    getInputPatternByPosition(position: number) :Pattern<T> {
        let map:Array<Pattern<T>> = [];
        let inputPattern: Pattern<T>;

        this.list.forEach(pattern => {
            let text = pattern.getText();
            text.split('').forEach(char => map.push(pattern));
        });

        let leftPatterns = map.slice(0, position).filter(pattern => pattern !== null).reverse();
        let rightPatterns = map.slice(position).filter(pattern => pattern !== null);

        let pattern = rightPatterns[0];
        if (pattern && pattern.canActivate()) {
            //Шаблон справа?
            inputPattern = pattern;
        } else if (leftPatterns[0] && leftPatterns[0].canActivate()){
            //Шаблон слева?
            pattern = leftPatterns[0];

            inputPattern = pattern.canActivate() ? pattern :  rightPatterns[0];
        } else {
            //Шаблон гдето правее...
            for (let i = 0; i < rightPatterns.length; i++) {
                if (rightPatterns[i].canActivate()) {
                    inputPattern = rightPatterns[i];
                    break;
                }
            }
        }
        return inputPattern;

    }

    getPatternByPositionRight( position: number):Pattern<T> {
        let patterns = this.getPatternsByPosition(position);
        return patterns[1] || patterns[0];
    }

    getPatternByPositionLeft(position: number):Pattern<T> {
        let patterns = this.getPatternsByPosition(position);
        return patterns[0] || patterns[1];
    }

    getPatternOffset(pattern: Pattern<T>): number {
        let offset = 0;
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i] === pattern) {
                break;
            }
            offset += this.list[i].getText().length;
        }

        return offset;
    }

    getInputPatternSelection():ITextSelection {

        let pattern = this.getInputPattern();

        let offset = this.getPatternOffset(pattern);
        return {
            start: offset,
            end: offset + pattern.getText().length
        }
    }

    getInputPattern():Pattern<T> {

        let inputPattern: Pattern<T> = null;

        for (let i = 0; i < this.list.length; i = i + 1) {
            let pattern = this.list[i];

            if (pattern.canActivate()) {
                inputPattern = pattern;
                break;
            }
        }

        return inputPattern;
    }

    getPatternsValue(): {[name:string]: any} {
        let map:{[name: string]: any} = {};
        this.list.forEach(pattern => {
            let name = pattern.getName();

            if (name) {
                map[name] = pattern.getValue();
            }
        });

        return map;
    }

    updateText() {
        this.list.forEach(pattern => pattern.updateText());
    }

    updateDisplay() {
        this.setActive(null);
        this.updateText();
    }

    dispose() {
        this.list.length = 0;
    }
}