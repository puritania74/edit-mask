import { API, IDateTimeMaskOptions, ITemplateMaskOptions } from "./API";
import { DateTimeEditor } from "./DateTimeEditor";
import { TemplateEditor } from "./TemplateEditor";
import { NumberEditor } from "./NumberEditor";


export class Mask {

    static api:API = new API();

    static dateTime(el: HTMLInputElement, template: string, options?: IDateTimeMaskOptions) {
        let mask = Mask.api.createDateTimeMask(template, options);
        return new DateTimeEditor(el, mask);
    }

    static template(el: HTMLInputElement, template: string, options?:ITemplateMaskOptions) {
        let mask = Mask.api.createTemplateMask(template, options);
        return new TemplateEditor(el, mask);
    }

    static number(el: HTMLInputElement, template: string) {
        let mask = Mask.api.createNumberMask(template);
        return new NumberEditor(el, mask);
    }

}

