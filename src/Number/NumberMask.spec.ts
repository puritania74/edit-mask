import 'mocha';
import * as chat from 'chai';
import { NumberMask } from "./NumberMask";
import { Locale } from "../locales/Locale";
import { NumberMaskBuilder } from "./builders/NumberMaskBuilder";

let assert = chat.assert;

describe('NumberMask', function () {

    let builder = new NumberMaskBuilder();
    let locale = new Locale('ru');

    describe('should correctly format', () => {
        let mask: NumberMask;

        beforeEach(() => {
            mask = <NumberMask>builder.build('n3', locale);
        });

        it('empty value', () => {
            assert.equal(mask.getText(), '_,___');
        });

        it('positive value', () => {
            mask.setValue(12.5436);
            assert.equal(mask.getText(), '12,544');
        });

        it('negative value', () => {
            mask.setValue(-12.5436);
            assert.equal(mask.getText(), '-12,544');
        });
    });

    describe('should correctly input number', () => {

        let mask: NumberMask = <NumberMask>builder.build('n3', locale);

        let input = [
            {'1': '1,000'},
            {'0': '10,000'},
            {'2': '102,000'},
            {'4': '1 024,000'},
            {',': '1 024,000'},
            {'7': '1 024,700'},
            {'6': '1 024,760'},
            {'8': '1 024,768'}
        ];

        checkInputText(mask, input, 1024.768);

    });

    describe('should correctly input percent', () => {

        let mask: NumberMask = <NumberMask>builder.build('p2', locale);

        let input = [
            {'3': '3,00 %'},
            {'3': '33,00 %'},
            {',': '33,00 %'},
            {'5': '33,50 %'},
            {'0': '33,50 %'}
        ];

        checkInputText(mask, input, 33.5);

    });

    describe('should correctly input percent with transform', () => {

        let mask: NumberMask = <NumberMask>builder.build('P2', locale);

        let input = [
            {'3': '3,00 %'},
            {'3': '33,00 %'},
            {',': '33,00 %'},
            {'5': '33,50 %'},
            {'0': '33,50 %'}
        ];

        checkInputText(mask, input, 0.335);
    });

    describe('should correctly input currency', () => {

        let mask: NumberMask = <NumberMask>builder.build('c2', locale);

        let input = [
            {'3': '3,00р.'},
            {'3': '33,00р.'},
            {',': '33,00р.'},
            {'5': '33,50р.'},
            {'0': '33,50р.'}
        ];

        checkInputText(mask, input, 33.5);
    });

    function checkInputText(mask: NumberMask, input: Array<{[char:string]: string}| number>, value: number) {
        let position = 0;
        let lastValue = null;

        mask.onChangeValue((event) => {
            lastValue = event.newValue;
        });

        input.concat(value).forEach(data => {
            if (typeof data === 'number') {
                it(`valid value ${data}`, () => {
                    assert.equal(mask.getValue(), data);
                    assert.equal(lastValue, data);
                });

                it(`valid last value ${data} in onChangeValue()`, () => {
                    assert.equal(lastValue, data);
                });
                return;
            }
            let char = Object.keys(data).pop();
            let text = data[char];

            it(`${char} change text to ${text}`, () => {
                let input = mask.getText();
                input = input.substring(0, position) + char + input.substring(position);
                position = mask.setInput(input, position + 1);
                assert.equal(mask.getText(), text);
            });

        });


    }


});
