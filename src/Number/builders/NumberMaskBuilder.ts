import { NumberMask } from "../NumberMask";
import { MaskBuilder } from "../../core/builders/MaskBuilder";
import { Locale } from "../../locales/Locale";
import { PatternsBuilder } from "../../core/builders/PatternsBuilder";
import { buildStaticPattern } from "../../common/StaticPatternBuilder";
import { RegExpSelector } from "../../core/selectors/RegExpSelector";
import { buildNumberPattern } from "./patterns/NumberPatternsBuilder";
import { buildCurrencyPattern } from "./patterns/CurrencyPatternBuilder";
import { buildPercentPattern } from "./patterns/PercentPatternBuilder";
import { buildPercentTransformedPattern } from "./patterns/PercentTransformedPatternBuilder";

export class NumberMaskBuilder extends MaskBuilder<number>{

    protected createMask(locale: Locale): NumberMask {
        return new NumberMask(locale);
    }

    protected initPatternsBuilder(): PatternsBuilder<number>{

        let patternsBuilder = new PatternsBuilder<any>(buildStaticPattern);

        return patternsBuilder
            .registerPattern(new RegExpSelector(/n\d*/i), buildNumberPattern)
            .registerPattern(new RegExpSelector(/c\d*/i), buildCurrencyPattern)
            .registerPattern(new RegExpSelector(/p\d*/), buildPercentPattern)
            .registerPattern(new RegExpSelector(/P\d*/), buildPercentTransformedPattern);
    }

}