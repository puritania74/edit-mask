import { NumberPattern } from "../../patterns/NumberPattern";
import { Locale } from "../../../locales/Locale";

export function buildNumberPattern( template:string, locale:Locale): NumberPattern {
    let regexp = /n(\d*)/i;
    let matches = template.match(regexp);
    let decimalDigits = matches[1];

    return new NumberPattern(locale, template, decimalDigits.length === 0 ? locale.numberDecimalDigits : parseInt(decimalDigits, 10))
}