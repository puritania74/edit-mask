import { Locale } from "../../../locales/Locale";
import { PercentPattern } from "../../patterns/PercentPattern";

export function buildPercentPattern( template:string, locale:Locale): PercentPattern {
    let regexp = /p(\d*)/;
    let matches = template.match(regexp);
    let decimalDigits = matches[1];

    return new PercentPattern(locale, template, decimalDigits.length === 0 ? locale.percentDecimalDigits: parseInt(decimalDigits, 10))
}