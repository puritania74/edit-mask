import { Locale } from "../../../locales/Locale";
import { PercentTransformedPattern } from "../../patterns/PercentTransformedPattern";

export function buildPercentTransformedPattern( template:string, locale:Locale): PercentTransformedPattern {
    let regexp = /P(\d*)/;
    let matches = template.match(regexp);
    let decimalDigits = matches[1];

    return new PercentTransformedPattern(locale, template, decimalDigits.length === 0 ? locale.percentDecimalDigits: parseInt(decimalDigits, 10))
}