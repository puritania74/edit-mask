import { CurrencyPattern } from "../../patterns/CurrencyPattern";
import { Locale } from "../../../locales/Locale";

export function buildCurrencyPattern( template:string, locale:Locale): CurrencyPattern{
    let regexp = /c(\d*)/i;
    let matches = template.match(regexp);
    let decimalDigits = matches[1];

    return new CurrencyPattern(locale, template, decimalDigits.length === 0 ? locale.currencyDecimalDigits : parseInt(decimalDigits, 10))
}