import { BaseNumberPattern } from "./BaseNumberPattern";
import { Locale } from "../../locales/Locale";
import { PercentParser } from "../../locales/parsers/PercentParser";
import { PercentFormatter } from "../../locales/formatters/PercentFormatter";

export class PercentPattern extends BaseNumberPattern {

    constructor(locale:Locale, template:string, decimalDigits:number) {
        super(locale, template, decimalDigits);
        this.parser = new PercentParser(locale);
        this.formatter = new PercentFormatter(locale);
    }

}