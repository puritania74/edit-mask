import { BaseNumberPattern } from "./BaseNumberPattern";
import { Locale } from "../../locales/Locale";
import { NumberParser } from "../../locales/parsers/NumberParser";
import { NumberFormatter } from "../../locales/formatters/NumberFormatter";

export class NumberPattern extends BaseNumberPattern {

    constructor(locale:Locale, template:string, decimalDigits:number) {
        super(locale, template, decimalDigits);
        this.parser = new NumberParser(locale);
        this.formatter = new NumberFormatter(locale);
    }


}