import { PercentPattern } from "./PercentPattern";
import { isEmpty } from "../../utils/isEmpty";

export class PercentTransformedPattern extends PercentPattern {

    applyValue(value: number) {
        if(!isEmpty(value) && isFinite(value) && !isNaN(value)) {
            value = value * 100;
        }

        super.applyValue(value);
    }

    getValue():number {
        let value = super.getValue();

        if(!isEmpty(value) && isFinite(value) && !isNaN(value)) {
            value = value / 100;
        }

        return value;
    }


}