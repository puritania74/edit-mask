import { BaseNumberPattern } from "./BaseNumberPattern";
import { Locale } from "../../locales/Locale";
import { CurrencyParser } from "../../locales/parsers/CurrencyParser";
import { CurrencyFormatter } from "../../locales/formatters/CurrencyFormatter";

export class CurrencyPattern extends BaseNumberPattern {

    constructor(locale:Locale, template:string, decimalDigits:number) {
        super(locale, template, decimalDigits);
        this.parser = new CurrencyParser(locale);
        this.formatter = new CurrencyFormatter(locale);
    }

}