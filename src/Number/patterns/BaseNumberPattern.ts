import { ISetInputCallback, Pattern } from "../../core/Pattern";
import { Locale } from "../../locales/Locale";
import { INumberFormatterOptions, NumberFormatter } from "../../locales/formatters/NumberFormatter";
import { IFormatter } from "../../locales/formatters/Formatter";
import { IParser } from "../../locales/parsers/base/interfaces";




export abstract class BaseNumberPattern extends Pattern<number> {

    protected isNegative: boolean = false;

    protected decimalDigits:number;

    protected parser: IParser;

    protected formatter: NumberFormatter;

    constructor(locale:Locale, template:string, decimalDigits:number) {
        super(locale, template, 'number');

        this.decimalDigits = decimalDigits;
    }

    protected setIsNegative(isNegative:boolean) {
        this.isNegative = isNegative;
    }

    protected getIsNegative():boolean {
        return this.isNegative;
    }

    toggleSign() {
        this.isNegative = !this.isNegative;
    }

    getIsEmpty(): boolean {
        let isEmpty;

        if (this.text === null || typeof this.text === 'undefined') {
            isEmpty = true
        } else {
            let digits = this.text.replace(/[^\d]/g, '');
            isEmpty = digits.length === 0;
        }

        return isEmpty;
    }

    getIsComplete(): boolean {
        return this.parser.isComplete(this.getText(), this.decimalDigits);
    }

    getIsValid(): boolean {
        return this.parser.isComplete(this.getText(), this.decimalDigits);
    }

    applyValue( value: number ) {
        this.setIsNegative(value < 0);
        let text = '';

        if (value !== null && typeof value !== 'undefined') {

            if (isFinite(value) && !isNaN(value)) {
                value = Math.abs(value);
                text = this.formatter.formatValue(value, {
                    decimalDigits: this.decimalDigits,
                    isNegative: this.getIsNegative(),
                    blank: this.getBlank()
                });

            }

        }
        this.setText(text);
    }

    getNextInput(): string {
        let text = this.getText();
        let next;
        let result = this.parser.parse(text, {
            decimalDigits: this.decimalDigits,
            isNegative: this.getIsNegative()
        }, 0);

        let value = parseInt(result.integralPart.replace(/[^\d]/g, ''), 10);

        if (isFinite(value) && !isNaN(value)) {
            value++;
            next = value.toString(10) + result.separatorPart + result.fractionalPart;
        } else {
            next = '0' + result.separatorPart + result.fractionalPart;
        }

        return next;
    }

    getPrevInput(): string {
        let text = this.getText();
        let prev;
        let result = this.parser.parse(text, {
            decimalDigits: this.decimalDigits,
            isNegative: this.getIsNegative()
        }, 0);

        let value = parseInt(result.integralPart.replace(/[^\d]/g, ''), 10);

        if (isFinite(value) && !isNaN(value)) {
            value--;
            prev = value.toString(10) + result.separatorPart + result.fractionalPart;
        } else {
            prev = '0' + result.separatorPart + result.fractionalPart;
        }

        return prev;
    }

    updateText() {
        let text = this.text || '';
        let formattedText;
        let formatterOptions:INumberFormatterOptions= {
            decimalDigits: this.decimalDigits,
            blank: this.getBlank(),
            isNegative: this.getIsNegative()
        };

        if (this.getIsEmpty()) {
            formattedText= this.formatter.formatEmpty(formatterOptions)
        } else if (this.isActive) {
            formattedText = this.formatter.formatInput(text, formatterOptions);
        } else {
            formattedText = this.formatter.format(text, formatterOptions);
        }

        this.setText(formattedText);
    }

    setInput( input: string, cb?: ISetInputCallback ): string {
        let applyText = null;
        let blank = this.getBlank();

        //strip leading blank chars
        while(input.indexOf(blank) === 0) {
            input = input.substring(blank.length);
        }

        let result = this.parser.parse(input, {
            isNegative: this.getIsNegative(),
            decimalDigits: this.decimalDigits
        }, 0);

        if (result.input.length === 0) {
            applyText = '';
        } else {
            // let separator = result.separatorPart || '';
            // if (result.fractionalPart.length > 0 && result.integralPart.length === 0) {
            //     result.integralPart = '0';
            // }
            applyText = result.integralPart + result.separatorPart + result.fractionalPart;
        }


        if (applyText !== null) {
            this.setText(applyText);
        }

        if (cb) {
            cb(input, applyText);
        }

        return input;
    }


    getValue():number {
        let text = this.getText();
        let result = this.parser.parse(text, {
            decimalDigits: this.decimalDigits,
            isNegative: this.getIsNegative()
        }, 0);

        let integralPart = result.integralPart.replace(/[^\d]/g, '');
        let fractionalPart = result.fractionalPart.replace(/[^\d]/g, '');
        let textValue = [integralPart, fractionalPart || '0'].join('.');

        let value:number = +textValue;


        if (isFinite(value) && !isNaN(value)) {
            if (this.getIsNegative()) {
                value = -value;
            }
        }

        return value;
    }



}







