import { Mask } from '../core/Mask';
import { NumberPattern } from './patterns/NumberPattern';
import { ITextSelection } from '../core/ITextSelection';
import { restoreNumberPrecision } from '../utils/restoreNumberPrecision';

export class NumberMask extends Mask<number> {

    protected buildValue(): number {
        let pattern = this.patterns.getInputPattern();

        return pattern.getValue();
    }

    handleKey(key: string, position: number): number {
        let newPosition: number = null;
        let text = this.getText();

        switch (key) {
            case '-':
                newPosition = this.tryToggleSign(position, text);
                break;
            case 'Backspace':
                newPosition = position === 1 && text.charAt(0) === '-'
                    ? this.tryToggleSign(position, text)
                    : newPosition;
                break;
            case 'Delete':
                newPosition = position === 0 && text.charAt(0) === '-'
                    ? this.tryToggleSign(position, text)
                    : newPosition;
                break;
        }

        return newPosition;
    }

    restorePosition(text: string, position: number): number {

        //@TODO Убрать из text groupSeparator

        let groupSeparator = this.locale.numberGroupSeparator;

        let left = text.substring(0, position);
        let right = text.substr(position);
        let pos = position;

        while (left.indexOf(groupSeparator) !== -1) {
            left = left.replace(groupSeparator, '');
            pos -= groupSeparator.length;
        }

        while (right.indexOf(groupSeparator) !== -1) {
            right = right.replace(groupSeparator, '');
        }

        return super.restorePosition(left + right, pos);
    }

    setNextPatternValue(position:number):ITextSelection {
        if (this.patterns.getIsValid()) {
            let value = this.value.getValue();
            let nextValue = restoreNumberPrecision(value, value + 1);
            this.value.setValue(nextValue, {update: true});
        }

        return this.patterns.getInputPatternSelection();
    }

    setPrevPatternValue(position:number):ITextSelection {
        if (this.patterns.getIsValid()) {
            let value = this.value.getValue();
            let prevValue = restoreNumberPrecision(value, value - 1);
            this.value.setValue(prevValue, {update: true});
        }

        return this.patterns.getInputPatternSelection();
    }

    getInvalidText() :string {
        let value = this.value.getValue();
        let notEmpty = typeof value !== 'undefined' && value !== null;
        let text = null;
        let locale = this.locale;

        if (notEmpty && isNaN(value)) {
            //NaN
            text = locale.NaNSymbol;
        } else if (notEmpty && !isFinite(value)) {
            //Infinity
            text = value === Number.POSITIVE_INFINITY ? locale.positiveInfinitySymbol : locale.negativeInfinitySymbol;
        } else {
            //none

        }

        return text;
    }

    updateInvalid() {
        let value = this.value.getValue();
        let notEmpty = typeof value !== 'undefined' && value !== null;

        if (notEmpty && isNaN(value)) {
            this.setInvalid(true);//NaN
        } else if (notEmpty && !isFinite(value)) {
            this.setInvalid(true);//Infinity
        } else {
            this.setInvalid(false)
        }

    }

    tryToggleSign(position: number, text: string): number | null {
        let pattern: NumberPattern = <NumberPattern>this.patterns.getInputPattern();

        if (!pattern) {
            return null;
        }

        pattern.toggleSign();
        this.patterns.updateText();
        let newPosition = this.restorePosition(text, position);

        this.updateValue();

        return newPosition;
    }

}