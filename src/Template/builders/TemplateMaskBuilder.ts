import { MaskBuilder } from "../../core/builders/MaskBuilder";
import { PatternsBuilder } from "../../core/builders/PatternsBuilder";
import { TemplateMask } from "../TemplateMask";
import { Locale } from "../../locales/Locale";
import { buildStaticPattern } from "../../common/StaticPatternBuilder";
import { RegExpSelector } from "../../core/selectors/RegExpSelector";
import { TemplatePattern } from "../patterns/TemplatePattern";
import { StringSelector } from "../../core/selectors/StringSelector";
import { DigitTemplatePattern } from "../patterns/DigitTemplatePattern";
import { SignTemplatePattern } from "../patterns/SignTemplatePattern";
import { CharTemplatePattern } from "../patterns/CharTemplatePattern";
import { LetterTemplatePattern } from "../patterns/LetterTemplatePattern";
import { AlphanumericTemplatePattern } from "../patterns/AlphanumericTemplatePattern";

export class TemplateMaskBuilder extends MaskBuilder<string> {

    protected initPatternsBuilder(): PatternsBuilder<string> {
        let patternsBuilder = new PatternsBuilder<any>(buildStaticPattern);


        return patternsBuilder
            .registerPattern(
                new StringSelector('#'),
                (template:string, locale:Locale) => new SignTemplatePattern(locale, template, false)
            )
            .registerPattern(
                new StringSelector('9'),
                (template:string, locale:Locale) => new DigitTemplatePattern(locale, template, false)
            )
            .registerPattern(
                new StringSelector('0'),
                (template: string, locale: Locale) => new DigitTemplatePattern(locale, template, true)
            )
            .registerPattern(
                new StringSelector('c'),
                (template: string, locale: Locale) => new CharTemplatePattern(locale, template, false)
            )
            .registerPattern(
                new StringSelector('C'),
                (template: string, locale: Locale) => new CharTemplatePattern(locale, template, true)
            )
            .registerPattern(
                new StringSelector('l'),
                (template: string, locale: Locale) => new LetterTemplatePattern(locale, template, false)
            )
            .registerPattern(
                new StringSelector('L'),
                (template: string, locale: Locale) => new LetterTemplatePattern(locale, template, true)
            )
            .registerPattern(
                new StringSelector('a'),
                (template: string, locale: Locale) => new AlphanumericTemplatePattern(locale, template, false)
            )
            .registerPattern(
                new StringSelector('A'),
                (template: string, locale: Locale) => new AlphanumericTemplatePattern(locale, template, true)
            );
    }


    protected createMask( locale: Locale ): TemplateMask {
        return new TemplateMask(locale);
    }

}