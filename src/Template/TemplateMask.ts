import { Mask } from "../core/Mask";
import { StaticPattern } from "../common/StaticPattern";
import { TemplatePattern } from "./patterns/TemplatePattern";


export class TemplateMask extends Mask<string> {

    protected maskSaveLiteral:boolean = true;


    setMaskSaveLiteral(maskSaveLiteral:boolean) {
        this.maskSaveLiteral = maskSaveLiteral;
    }

    applyValue(value: string) {
        value = value || '';
        let patterns = this.patterns;
        let list = patterns.getList();

        if (this.getMaskSaveLiteral()) {
            //Маска сохраняет литералы в значении -
            list.forEach((pattern, i) => pattern.applyValue(value.substr(i, 1)));

        } else {
            //Маска НЕ сохраняет литералы в значении
            let i = 0;
            list
                .filter(pattern => !(pattern instanceof StaticPattern))
                .forEach(pattern => pattern.applyValue(value.substr(i++, 1)))

        }

        this.patterns.updateText();
    }

    getMaskSaveLiteral() :boolean {
        return this.maskSaveLiteral;
    }

    protected buildValue(): string {
        return this.patterns.getList()
            .filter(pattern => this.getMaskSaveLiteral() ? true : pattern instanceof TemplatePattern)
            .map(pattern => pattern.getText())
            .join('');
    }


}