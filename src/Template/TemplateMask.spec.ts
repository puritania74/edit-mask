import 'mocha';
import * as chat from 'chai';
import { Locale } from "../locales/Locale";
import { TemplateMaskBuilder } from "./builders/TemplateMaskBuilder";
import { TemplateMask } from "./TemplateMask";

let assert = chat.assert;

describe('TemplateMask', function () {

    let builder = new TemplateMaskBuilder();
    let locale = new Locale('ru');

    describe('should correctly format', () => {
        let mask: TemplateMask;

        beforeEach(() => {
            mask = <TemplateMask>builder.build('(000)000-00-00', locale);
        });

        it('empty value', () => {
            assert.equal(mask.getText(), '(___)___-__-__');
        });

        it('text value', () => {
            mask.setValue('(987)654-32-10');
            assert.equal(mask.getText(), '(987)654-32-10');
        });

        it('text value without MaskSaveLiteral', () => {
            mask.setMaskSaveLiteral(false);
            mask.setValue('9876543210');
            assert.equal(mask.getText(), '(987)654-32-10');
        });
    });

    describe('should correctly input text', () => {

        let mask: TemplateMask = <TemplateMask>builder.build('(000)000-00-00', locale);
        mask.setMaskSaveLiteral(false);

        let input = [
            {'9': '(9__)___-__-__'},
            {'8': '(98_)___-__-__'},
            {'7': '(987)___-__-__'},
            {'6': '(987)6__-__-__'},
            {'5': '(987)65_-__-__'},
            {'4': '(987)654-__-__'},
            {'3': '(987)654-3_-__'},
            {'2': '(987)654-32-__'},
            {'1': '(987)654-32-1_'},
            {'0': '(987)654-32-10'},
        ];

        checkInputText(mask, input, '9876543210');
    });

    function checkInputText(mask: TemplateMask, input: Array<{[char:string]: string}| string>, value: string) {
        let position = 0;
        let lastValue = null;

        mask.onChangeValue((event) => {
            lastValue = event.newValue;
        });

        input.concat(value).forEach(data => {
            if (typeof data === 'string') {
                it(`valid value ${data}`, () => {
                    assert.equal(mask.getValue(), data);
                    assert.equal(lastValue, data);
                });

                it(`valid last value ${data} in onChangeValue()`, () => {
                    assert.equal(lastValue, data);
                });
                return;
            }
            let char = Object.keys(data).pop();
            let text = data[char];

            it(`${char} change text to ${text}`, () => {
                let input = mask.getText();
                input = input.substring(0, position) + char + input.substring(position);
                position = mask.setInput(input, position + 1);
                assert.equal(mask.getText(), text);
            });

        });


    }

});
