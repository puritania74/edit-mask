import { TemplatePattern } from "./TemplatePattern";
import { Locale } from "../../locales/Locale";

export class AlphanumericTemplatePattern extends TemplatePattern {

    protected regexpLetter:RegExp;

    constructor(locale:Locale, template:string, isRequired:boolean) {
        super(locale, template, isRequired);
        this.regexpLetter = new RegExp('[0-9' + locale.letter + ']');
    }

    validateInput( input: string ): boolean {
        return this.regexpLetter.test(input);
    }

}