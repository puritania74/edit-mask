import { TemplatePattern } from "./TemplatePattern";
import { Locale } from "../../locales/Locale";

export class LetterTemplatePattern extends TemplatePattern {

    protected regexpLetter:RegExp;

    constructor(locale:Locale, template:string, isRequired:boolean) {
        super(locale, template, isRequired);
        this.regexpLetter = new RegExp('[' + locale.letter + ']');

    }

    validateInput( input: string ): boolean {
        return this.regexpLetter.test(input);
    }

}