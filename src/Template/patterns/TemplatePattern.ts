import { ISetInputCallback, Pattern } from "../../core/Pattern";
import { Locale } from "../../locales/Locale";

export abstract class TemplatePattern extends Pattern<string> {

    protected isRequired:boolean = false;

    constructor(locale:Locale, template:string, isRequired:boolean) {
        super(locale, template);
        this.isRequired = isRequired;
    }

    getIsRequired():boolean {
        return this.isRequired;
    }

    getIsComplete(): boolean {
        return !this.getIsEmpty() ;
    }

    getIsEmpty(): boolean {
        let isEmpty;
        let text = this.text;

        if (text === null || typeof text === 'undefined' || text === '' || text.length === 0) {
            isEmpty = true
        } else {
            isEmpty = this.cleanBlank(text).length === 0;
        }

        return isEmpty;
    }

    cleanBlank(text:string):string {
        return super.cleanBlank(text).replace(/\s/g, '');
    }

    updateText() {
        let text:string;

        if (this.getIsEmpty()) {
            text = this.getBlank();
        } else  {
            text = this.cleanBlank(this.text).substr(0, 1);
        }
        this.setText(text);
    }

    setInput( input: string, cb?: ISetInputCallback ): string {
        let applyText = null;

        input = this.trimBlank(input);

        let test = input.substr(0, 1);

        if (this.validateInput(test)) {
            this.setText(test);
            applyText = test;
            input = input.substr(test.length);
        } else if(!this.getIsRequired() || test.length === 0) {
            applyText = '';
            this.setText(applyText);
        } else {
            //Обязательное, но подходящего значения нет
        }

        if (cb) {
            cb(input, applyText);
        }

        return input;
    }


    applyValue( value: string) {
        let char = value;
        let blank = this.getBlank();

        if (char === blank) {
            this.setText('')
        } else if (this.validateInput(char)) {
            this.setText(char);
        } else {
            //Текст не подходит маске - не меняем значение
        }
    }

    getIsValid(): boolean {
        return !this.getIsRequired() || !this.getIsEmpty();
    }

    abstract validateInput(input: string):boolean


}