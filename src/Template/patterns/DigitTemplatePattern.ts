import { TemplatePattern } from "./TemplatePattern";

export class DigitTemplatePattern extends TemplatePattern {

    validateInput( input: string ): boolean {
        return /\d/.test(input);
    }

}