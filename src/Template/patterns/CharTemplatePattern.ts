import { TemplatePattern } from "./TemplatePattern";

export class CharTemplatePattern extends TemplatePattern {

    validateInput( input: string ): boolean {
        return /^.*$/.test(input);
    }

}