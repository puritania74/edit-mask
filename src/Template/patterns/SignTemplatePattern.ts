import { TemplatePattern } from "./TemplatePattern";

export class SignTemplatePattern extends TemplatePattern {

    validateInput( input: string ): boolean {
        return /^[\d+-]$/.test(input);
    }

}