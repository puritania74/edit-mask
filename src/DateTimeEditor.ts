import { Editor } from "./Editor";
import { DateTimeMask } from "./DateTime/DateTimeMask";

export class DateTimeEditor extends Editor<number>{

    setTimezoneOffset(offset: number){
        return this.getMask().setTimezoneOffset(offset);
    }

    getTimezoneOffset() :number {
        return this.getMask().getTimezoneOffset();
    }

    setMinValue(minValue: number) {
        this.getMask().setMinValue(minValue);
    }

    getMinValue() : number {
        return this.getMask().getMinValue();
    }

    setMaxValue(maxValue: number) {
        this.getMask().setMaxValue(maxValue);
    }

    getMaxValue() : number {
        return this.getMask().getMaxValue();
    }

    protected getMask(): DateTimeMask {
        return <DateTimeMask>this.mask;
    }

}


