import { ISetInputCallback, Pattern } from "../core/Pattern";
export class StaticPattern extends Pattern<any> {

    getIsComplete(): boolean {
        return true;
    }

    getIsValid(): boolean {
        return true;
    }

    applyValue( value: any ) {
        //do nothing
    }


    getIsEmpty(): boolean {
        //Статичный текст не должен влиять на статус "заполнен" маски ввода
        return true;
    }


    setInput( input: string, cb? : ISetInputCallback ): string {
        let text = this.getText();
        let applyText = '';

        let index = input.indexOf(text);
        if (index !== -1) {
            applyText = text;
            input = input.substring(0, index) + input.substring(index + text.length);
        }

        if (cb) {
            cb(input, applyText);
        }

        return input;
    }

    updateText() {
        //@TODO Добавить замену разделителей согласно локали
        this.text = this.template;
    }

    canActivate():boolean {
        return false;
    }


}