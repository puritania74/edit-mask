import { padEnd } from "../utils/padEnd";
import { padStart } from "../utils/padStart";
import { DecimalPattern, DecimalPatternOptions } from "./DecimalPattern";

export interface FixedWidthPatternOptions extends DecimalPatternOptions{
    width: number
}

export abstract class FixedWidthPattern<T> extends DecimalPattern<T> {

    protected width: number;

    constructor(locale, template: string, name: string, options: FixedWidthPatternOptions) {
        super(locale, template, name, options);
        this.width = options.width;
    }

    getIsComplete(): boolean {
        return this.getIsValid();
    }

    getIsValid(): boolean {
        let text = this.getText();
        let digits = text.replace(/[^\d]/g, '');
        let value = parseInt(digits, 10);

        return value >= this.min && digits.length === this.width;
    }

    updateText() {
        let text = this.text || '';
        let digits = text.replace(/[^\d]/g, '');

        if (this.getIsEmpty()) {
            text = padEnd(digits, this.width, this.getBlank())
        } else {
            text = this.isActive ? padEnd(digits, this.width, this.getBlank()) : this.formatDecimal(digits);
        }

        this.setText(text);
    }

    getInputRegexp():RegExp {
        //fetch first "width" digits number that less or equal "max"
        return new RegExp('^[\\d]{1,' + this.width + '}');
    }

    finalizeInput():boolean {
        let finalize:boolean = false;

        if (this.getIsComplete()) {
            let text = this.getText();
            let digits = text.replace(/[^\d]/g, '');
            if (digits.length < this.width) {
                this.setText(this.formatDecimal(digits));
                finalize = true;
            }
        }

        return finalize;
    }

    formatDecimal(value: string) {
        return padStart(value, this.width, '0')
    }


}