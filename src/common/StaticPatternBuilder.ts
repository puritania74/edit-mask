import { StaticPattern } from "./StaticPattern";
import { Locale } from "../locales/Locale";

export function buildStaticPattern( template:string, locale:Locale): StaticPattern{
    return new StaticPattern(locale, template);
}