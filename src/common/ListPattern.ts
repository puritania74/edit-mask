import { ISetInputCallback, Pattern } from "../core/Pattern";
import { padEnd } from "../utils/padEnd";

export abstract class ListPattern<T> extends Pattern<T> {

    protected list:Array<string>;

    protected minWidth:number;

    constructor(locale, template:string, name:string, list:Array<string>) {
        super(locale, template, name);
        this.list = list;
        this.minWidth = list.map(t => t.length).sort()[0];
    }

    getIsEmpty(): boolean {
        let isEmpty;
        let text = this.text;

        if (text === null || typeof text ==='undefined') {
            isEmpty = true;
        } else {
            let blank = this.getBlank();
            while(text.indexOf(blank) !== -1) {
                text = text.replace(blank, '');
            }
            isEmpty = text.length === 0;
        }

        return isEmpty;
    }

    getNextInput(): string {
        let text = this.getText();
        let next = text;

        let i = this.list.map(t => t.toUpperCase()).indexOf(text.toUpperCase());

        if (i === -1) {
            next = this.list[0];
        } else if (i + 1 < this.list.length) {
            next = this.list[i + 1];
        }

        return next;
    }

    getPrevInput(): string {
        let text = this.getText();
        let prev = text;

        let i = this.list.map(t => t.toUpperCase()).indexOf(text.toUpperCase());

        if (i === -1) {
            prev = this.list[0];
        } else if (i - 1 >= 0) {
            prev = this.list[i - 1];
        }

        return prev;
    }

    getIsValid(): boolean {
        let text = this.getText().toUpperCase();

        return this.list
            .map(t => t.toUpperCase())
            .some(t => t === text);
    }

    getIsComplete(): boolean {
        return this.getIsValid();
    }

    updateText() {
        let text = this.text || '';
        let width = this.minWidth;

        if (!this.getIsEmpty()) {
            //Вычислить макс ширину слова, подходящего для уже введенного текстаъ
            let txt = text.toUpperCase();
            let longestText = this.list.map(t => t.toUpperCase())
                .filter(t => t.indexOf(txt) !== -1)
                .sort((a, b) => a.length - b.length)
                .pop();

            if (typeof longestText !== 'undefined') {
                width = longestText.length;
            }
        }

        text = padEnd(text, width, this.getBlank());

        this.setText(text);
    }

    setInput( input: string, cb?: ISetInputCallback ): string {
        let applyText = null;

        if (input.length === 0) {
            applyText = ''
        } else {

            let list = this.list
                .map(t => t.toUpperCase())
                .sort((a, b) => a.length - b.length);

            let width = Math.max(...list.map(t => t.length));

            let inputs = [];

            for (let i = 1; i <= Math.min(width, input.length); i = i + 1) {
                let test = input.substr(0, i);

                if(list.some(value => value.substr(0, i) === test.toUpperCase())) {
                    inputs.push(test);
                }
            }

            let len = inputs.sort((a, b) => a.length - b.length).map(a => a.length).pop();
            if (typeof len === 'undefined') {
                applyText = '';
            } else {
                applyText = input.substr(0, len);
                input = input.substr(len + 1);
            }

        }

        if (applyText !== null) {
            this.setText(applyText);
        }


        if (cb) {
            cb(input, applyText);
        }

        return input;

    }

    getValue():number {
        let index = this.list.map(text => text.toUpperCase()).indexOf(this.getText().toUpperCase());

        return index === -1 ? null : index;
    }

    finalizeInput():boolean {
        let finalized: boolean = false;
        let text = this.getText().toUpperCase();

        let matched:Array<string> = this.list.filter(t => t.toUpperCase().indexOf(text) === 0);

        if (matched.length === 1) {
            this.setText(matched[0]);
            finalized = true;
        }

        return finalized;
    }
}