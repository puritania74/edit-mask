import { DecimalPattern, DecimalPatternOptions } from "./DecimalPattern";
import { padEnd } from "../utils/padEnd";
import { padStart } from "../utils/padStart";
export interface FloatWidthPatternOptions extends DecimalPatternOptions{
    minWidth: number
    maxWidth: number
}

export abstract class FloatWidthPattern<T> extends DecimalPattern<T> {

    minWidth: number;
    maxWidth: number;

    constructor(locale, template:string, name: string, options: FloatWidthPatternOptions) {
        super(locale, template, name, options)
        this.minWidth = options.minWidth;
        this.maxWidth = options.maxWidth;
    }

    updateText() {
        let text = this.text || '';
        let digits = text.replace(/[^\d]/g, '');

        if (this.getIsEmpty()) {
            text = padEnd('', this.minWidth, this.getBlank())
        } else if (this.isActive){
            text = padEnd(digits, this.minWidth, this.getBlank());
        } else {
            text = padEnd(digits.replace(/^0+/, '0'), this.minWidth, this.getBlank())
        }

        this.setText(text);
    }

    getInputRegexp():RegExp {
        //fetch first digits number
        return /^\d+/;
    }

}