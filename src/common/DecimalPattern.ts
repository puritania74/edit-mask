import { ISetInputCallback, Pattern } from "../core/Pattern";
import { padEnd } from "../utils/padEnd";
import { padStart } from "../utils/padStart";

export interface DecimalPatternOptions {
    min: number
    max: number
}

export abstract class DecimalPattern<T> extends Pattern<T> {

    protected min: number;
    protected max: number;

    constructor(locale, template: string, name: string, options: DecimalPatternOptions) {
        super(locale, template, name);
        this.min = options.min;
        this.max = options.max;
    }

    getIsEmpty(): boolean {
        let isEmpty;

        if (this.text === null || typeof this.text === 'undefined') {
            isEmpty = true
        } else {
            let digits = this.text.replace(/[^\d]/g, '');
            isEmpty = digits.length === 0;
        }

        return isEmpty;
    }

    getIsValid(): boolean {
        let text = this.getText();
        let digits = text.replace(/[^\d]/g, '');
        let month = parseInt(digits, 10);

        return month >= this.min && month <= this.max;
    }

    getNextInput() :string{
        let next;
        if (this.getIsEmpty()) {
            next = this.min.toString(10);
        } else {
            let text = this.getText();
            let data = parseInt(text.replace(/[^\d]/g, ''), 10);
            if (data < this.max) {
                next = '' + ++data;
            } else {
                next = text;
            }
        }

        return next;
    }

    getPrevInput(): string {
        let next;
        if (this.getIsEmpty()) {
            next = this.min.toString(10);
        } else {
            let text = this.getText();
            let data = parseInt(text.replace(/[^\d]/g, ''), 10);
            if (data > this.min) {
                next = '' + --data;
            } else {
                next = text;
            }
        }

        return next;
    }

    canInputChar(char:string):boolean {
        //can input only digits
        return /\d/.test(char);
    }

    protected abstract getInputRegexp():RegExp

    setInput( input: string, cb? : ISetInputCallback): string {
        let applyText = null;

        input = this.trimBlankLeft(input);

        let matches = input.match(this.getInputRegexp());

        if (input.length === 0) {
            applyText = ''
        } else  if (matches) {
            let digits = matches[0];

            let data = digits.split('')
                .map((digit, i, digits) => digits.slice(0, i + 1).join(''))
                .filter(month => parseInt(month, 10) <= this.max)
                .pop();

            if (data.length) {
                applyText = data;
                input = input.substring(matches.index + data.length);
            }
        }
        if (applyText !== null) {
            this.setText(applyText);
        }

        if (cb) {
            cb(input, applyText);
        }

        return input;
    }

    getValue():number {
        let text = this.text || '';

        let digits = text.replace(/[^\d]/g, '');

        return digits.length === 0 ? null : parseInt(digits, 10);
    }
}