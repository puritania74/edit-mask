import { Editor } from "./Editor";
import { TemplateMask } from "./Template/TemplateMask";

export class TemplateEditor extends Editor<string>{

    setMaskSaveLiteral(maskSaveLiteral:boolean) {
        this.getMask().setMaskSaveLiteral(maskSaveLiteral);
    }

    getMaskSaveLiteral():boolean {
        return this.getMask().getMaskSaveLiteral();
    }

    protected getMask(): TemplateMask {
        return <TemplateMask>this.mask;
    }
}