import { Editor } from "./Editor";

export class NumberEditor extends Editor<number>{

    protected onKeydownHandler(event) {
        super.onKeydownHandler(event);

        if (event.altKey || event.ctrlKey || event.shiftKey) {
            return;
        }

        let position = this.el.selectionStart;

        switch(event.key) {
            case '-':
            case 'Backspace':
            case 'Delete':
                position = this.mask.handleKey(event.key, position);
                if (position !== null) {
                    event.preventDefault();
                    this.updateInputElement(position);
                }
                break;
        }
    }

}