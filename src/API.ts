import { Builder } from "./core/builders/Builder";
import { Locale } from "./locales/Locale";
import { TemplateMask } from "./Template/TemplateMask";
import { DateTimeMaskBuilder } from "./DateTime/builders/DateTimeMaskBuilder";
import { NumberMaskBuilder } from "./Number/builders/NumberMaskBuilder";
import { TemplateMaskBuilder } from "./Template/builders/TemplateMaskBuilder";
import { DateTimeMask } from "./DateTime/DateTimeMask";
import { NumberMask } from "./Number/NumberMask";

export interface IConfig {
    locale: string
}

export interface IMaskOptions {

}

export interface ITemplateMaskOptions extends IMaskOptions {
    maskSaveLiteral: boolean
}

export interface IDateTimeMaskOptions extends IMaskOptions {
    timezoneOffset: number
}

const DEFAULT_LOCALE_NAME = 'en';

const DATETIME_MASK = 'DateTime';
const NUMBER_MASK = 'Number';
const TEMPLATE_MASK = 'Template';


export class API {

    protected builder = new Builder();

    protected options:IConfig = {
        locale: DEFAULT_LOCALE_NAME
    };

    protected locale:Locale;

    protected initialized: boolean = false;

    constructor() {
        this.builder.register('DateTime', new DateTimeMaskBuilder());
        this.builder.register('Number', new NumberMaskBuilder());
        this.builder.register('Template', new TemplateMaskBuilder());
    }

    init(options?: IConfig) {

        if (this.initialized) {
            return;
        }

        if (options) {
            Object.keys(options).forEach(key => this.options[key] = options[key]);
        }

        this.locale = new Locale(this.options.locale);

        this.initialized = true;
    }

    getLocale() : Locale {
        this.initialize();
        return this.locale;
    }

    createDateTimeMask(template:string, options?:IDateTimeMaskOptions):DateTimeMask {
        this.initialize();
        let mask:DateTimeMask = <DateTimeMask>this.createMask(DATETIME_MASK, template);

        options = options || <IDateTimeMaskOptions>{};
        if (typeof options.timezoneOffset !== 'undefined') {
            mask.setTimezoneOffset(options.timezoneOffset);
        }

        return mask;
    }

    createNumberMask(template:string):NumberMask {
        this.initialize();
        return <NumberMask>this.createMask(NUMBER_MASK, template);
    }

    createTemplateMask(template:string, options?:ITemplateMaskOptions):TemplateMask {
        this.initialize();

        options = options || <ITemplateMaskOptions>{};

        let mask:TemplateMask = <TemplateMask>this.createMask(TEMPLATE_MASK, template, options);
        if(typeof options.maskSaveLiteral !== 'undefined') {
            mask.setMaskSaveLiteral(options.maskSaveLiteral);
        }

        return mask;
    }

    protected createMask(name:string, template:string, options?: IMaskOptions) {
        return this.builder.build(name, template, this.locale);
    }

    protected initialize() {
        this.init();
    }
}
